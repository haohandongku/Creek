/*设置应用根目录*/
process.env.NODE_creek_ROOT = __dirname;
/*获取应用运行模式*/
var runPattern = process.env.NODE_ENV;
if(runPattern !== 'production'){
  runPattern = 'development';
}

console.log('\b进程[', process.pid, ']运行环境为: ', runPattern);

/*加载应用所需的模块*/
var sConfFile = '/creek-config.js';
var express = require('express');
var path = require('path');
var logger = require('creek-logger');
var creekUtils = require('creek-utils');
var oMiddlewareSwitch = creekUtils.getJsonProp(sConfFile,'middleware-switch') || {};

/*应用服务器实例*/
var app = express();

/*模板引擎配置*/
app.set('views', path.join(__dirname, 'views'));
/*开发环境下的静态文件服务配置*/
if(runPattern == 'development'){
  app.set('view cache', false);
}else{
  app.set('view cache', true);
}
app.set('view engine', 'tpl');
app.engine('tpl', require('hbs').__express);

/*应用网关*/
if (fnIfUseMiddleware('creek-getway')) {
  require('creek-getway')(app);
}

/*站点logo服务*/
if (fnIfUseMiddleware('serve-favicon')) {
  app.use(require('serve-favicon')(path.join(__dirname, 
    creekUtils.getJsonProp(sConfFile, 'serve-favicon.icon-path'))));
}

/*记录请求日志的中间件*/
if (fnIfUseMiddleware('creek-middleware-logger')) {
  app.use(require('creek-middleware-logger'));
}

/*解析请求体的中间件，必须开启*/
var bodyParser = require('body-parser');
app.use(bodyParser.json(creekUtils.getJsonProp(sConfFile, 'body-parser.json')));
app.use(bodyParser.urlencoded(creekUtils.getJsonProp(sConfFile, 'body-parser.urlencoded')));
app.use(bodyParser.text(creekUtils.getJsonProp(sConfFile, 'body-parser.text')));

/*解析cookie的中间件*/
if(fnIfUseMiddleware('cookie-parser')){
  app.use(require('cookie-parser')());
}

/*解析session的中间件*/
if (fnIfUseMiddleware('creek-session')) {
  app.use(require('creek-session')());
}

/*单点登录服务器*/
if (fnIfUseMiddleware('creek-sso-server')) {
  require('creek-sso-server')(app);/*支持单点登录的中间件*/
}

/*单点登录客户端*/
if (fnIfUseMiddleware('creek-sso-client')) {
  require('creek-sso-client')(app);/*支持单点登录的中间件*/
}

/*安全中间件*/
if (fnIfUseMiddleware('creek-security')) {
  var sMountPath = creekUtils.getJsonProp(sConfFile, 'creek-security.mount-path');
  sMountPath = sMountPath || '/apps';
  app.use(sMountPath, require('creek-security')());
}

/*加载数据代理*/
global.dataproxy = require('creek-data-proxy');

/*浏览器ajax统一入口*/
app.use(creekUtils.getJsonProp(sConfFile, 'creek-ajax-proxy.mount-path'), require('creek-ajax-proxy'));

/*加载应用特有的配置*/
require('./conf/app-local.js')(app, runPattern);

/*静态文件服务器配置*/
app.use('/static',express.static(path.join(__dirname, 'static')));
app.use('/s_p', express.static(path.join(__dirname, 's_p')));
app.use(express.static(path.join(__dirname, 'html/web')));
app.use(express.static(path.join(__dirname, 'html')));

// 处理404
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// 错误处理
app.use(function(err, req, res, next){
  if (err && err.code == 'EPERM') {
    logger.warn('app.js|发生 EPERM 错误|该异常是开发环境下读取session文件时产生的, 可以忽略.');
    next();
    return;
  }

  logger.error('捕获express异常');

  err = err ||{};
  err.status = err.status || 500;
  logger.error(err);

  res.status(err.status);

  var oErrorPath = creekUtils.getJsonProp(sConfFile, 'error-path');
  if (err.status == 404) {
    logger.error('404|路径未找到|', req.originalUrl);

    res.render(oErrorPath['404'] || '404/404');
    return;
  }
  res.render(oErrorPath['500'] || '500/500.tpl');
});
//未捕获的异常处理
app.use(function(err, req, res, next){
  logger.error('未捕获的express异常');
  logger.error(err);
  res.send('服务器发生未知错误，请与管理员联系。');
});

/*处理进程未捕获的异常*/
process.on('uncaughtException', function(err){
  logger.error('未捕获的进程异常|uncaughtException');
  logger.error(err);
});

module.exports = app;

function fnIfUseMiddleware(sName){
  return (oMiddlewareSwitch[sName] || (oMiddlewareSwitch[sName] === undefined));
}