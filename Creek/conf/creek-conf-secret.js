/**
 - 该文件用于项目在运行期间涉及到的私密信息的配置，例如密码等。
 - @author 康永胜
 - @date   2017-02-17 15:08:13
 */
var oAppConfig = {
  // 'redis': {
  //   'pass': 'foobared'
  // },
  // 'creek-sso-client': {
  //   'session-center-redis': {
  //     'pass': 'foobared'
  //   }
  // }
};

module.exports = oAppConfig;