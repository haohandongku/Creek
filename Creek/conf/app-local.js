var express = require('express');
/**
 * 请将所有属于本系统的路由等信息放置在该函数中。
 * @author wuche
 * @date   2016-08-31T16:42:25+0800
 * @param  {Object}     app        [应用句柄]
 * @param  {String}     runPattern [当前系统的运行模式，'development'或'production']
 * @return {undefined}             []
 */
module.exports = function(app, runPattern){
  /*首页模块的路由*/
  app.use('/', require('..' + __uri('/web/creek/index.routes.js')));
   '__components_routes__'
};