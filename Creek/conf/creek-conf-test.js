var oAppConfig = {
  /*服务器端口号*/
  'server-port': 3000,

  'deploy': {
    'cdn-public': {
      'url': 'http://10.126.3.19:3001/creek',
      'receivers': [
        {
          'url' : 'http://10.126.3.19:3003/receiver',
          'real-path' : '/u01/cdn_public/public/creek'
        }
      ]
    },
    'cdn-private':{
      'url': 'http://10.126.3.19:3002/creek',
      'receivers': [
        {
          'url' : 'http://10.126.3.19:3003/receiver',
          'real-path' : '/u01/cdn_private/public/creek'
        }
      ]
    },
    'creek-server': {
      'receivers': [
        {
          'url' : 'http://10.126.3.19:3003/receiver',
          'real-path' : '/u01/creek/creek_server'
        }
      ]
    }
  },

  'middleware-switch': {
    'creek-sso-client': true
  },

  'redis': {
    'host': '10.126.3.114',
    'port': 6379,
    'pass': '3112a1e4c2ea11e4:hlDp687FMSVHzeomrnvQJXu4LCBZgP',
    'db': 4
  },

  'creek-data-proxy': {
    'data-sources': {
      'default': {
        'description': '默认数据源',
        'urls': ['http://127.0.0.1:8080/adp/'],
        'default-path': '/servlet/webServlet',
        'pulse-path': '/loginsuccess.html',
        'props-map':['service -> serviceName', 'method -> methodName', 'data -> requestJson']
      }
    }
  },
  'creek-session': {
    'name': 'creek.connect.sid',
  },
  'creek-sso-client': {
    'system-name': 'creek-server', 
    'creek-sso-server-uri': 'http://10.126.3.19:3040',
    'session-center-redis': {
      'host': '10.126.3.114',
      'pass': '3112a1e4c2ea11e4:hlDp687FMSVHzeomrnvQJXu4LCBZgP',
      'db': 1, 
      'session-renewal-seconds': 1800
    }
  },

  'creek-security': {
    'path-pattern': 'mount',
    'validate-rules': [
      {
        'roleList': ['default'],
        'patternList': ['/**']
      }
    ]
  }
};

module.exports = oAppConfig;