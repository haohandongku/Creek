var oAppConfig = {
  'middleware-switch': {
    'creek-sso-client': true
  },

  'deploy': {
    // 'real-path': 'D:/WS_Node/szyt/creek-server-deploy'
  },
  'creek-staticize': {
    'disabled': true
  },
  'creek-data-proxy': {
    'data-sources': {
      'default': {
        'description': '默认数据源',
        'urls': ['http://127.0.0.1:8080/adp/'],
        'default-path': '/servlet/webServlet',
        'pulse-path': '/loginsuccess.html',
        'props-map':['service -> serviceName', 'method -> methodName', 'data -> requestJson']
      }
    }
  },

  'creek-session': {
    'name': 'creek.connect.sid'
  },

  'creek-sso-client': {
    'system-name': 'http://127.0.0.1:3000/login', 
    'creek-sso-server-uri': 'http://10.126.3.15:3040/',
    'session-center-redis': {
      'host': '10.126.3.15',
      'pass': '3112a1e4c2ea11e4:hlDp687FMSVHzeomrnvQJXu4LCBZgP',
      'db': 2,
      'session-renewal-seconds': 60 * 60 * 2,//两小时
    }
  },

  'creek-security': {
    'path-pattern': 'mount',
    'validate-rules': [
      {
        'roleList': ['default'],
        'patternList': ['/**']
      }
    ]
  }
};

module.exports = oAppConfig;