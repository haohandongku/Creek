# creek-server

## 功能描述
creek大前端开发平台的入口模块。

## 使用方式

## 配置信息

## 版本更新记录

### v2.5.2
发布时间: 2017-07-25 15:11:57
变更说明:
  1. 在 `/conf/app-local.js` 文件中加入了 `deploy.uri-to-open-after-running` 和 `deploy.application-to-open-uri` 两个配置项的示例。
  2. 修改了在 `creek-conf-development.js` 文件的 `creek-sso-client.system-name` 项目。

### v2.5.1
发布时间: 2017-06-08 10:59:39
变更说明:
  1. 在 `/conf/app-local.js` 文件中加入了 `__components_routes__` 字符串, 当 `creek-cli` 的版本大于等于 `0.26.0` 时会将组件的路由默认挂载到 `__components_routes__` 字符串所在的位置。
  2. 在 `creek-conf-development.js` 文件加入了关闭静态化的配置，在开发模式下默认关闭静态化。

### v2.5.0
发布时间: 2017-05-16 15:00:23
变更说明:
  1. 增加了对组件 `mixins` 和 `vars` 的默认引用。
 
### v2.4.0
发布时间: 2017-04-20 16:56:22
变更说明:
  1. 框架在生产环境下也支持 `static` 目录下的静态文件服务。
  2. 完善了demo，增加了静态化示例。

### v2.3.0
发布时间: 2017-04-05 14:34:34
变更说明:
  1. 框架开始默认支持 `text/plain` 类型的请求。
  2. 请求体的配置参数可以在 `creek-config-**.js` 文件中进行配置了。
  3. 根据新版的 `app` 模块更新了示例。

### v2.2.0
发布时间: 2017-03-14 10:21:15
变更说明:
  1. `creek-ajax-prody` 模块的引用升级到了 **2*。
 
### v2.1.0
发布时间: 2017-03-14 10:21:15
变更说明:
  1. 增加了异步依赖的示例。
  2. 对 `creek-data-proxy` 的依赖升级为 `~2`。
  3. 对 `seajs` 的依赖升级为 `3.0.0-creek.1`。
 
### v2.0.0
发布时间: 2017-03-10 10:21:59
变更说明:
  1. 删除了`/static`和`/components`目录。
  2. 所有的配置文件都防止到了`/conf`目录中。
  3. 将 `fis-conf.js` 文件移动到 `creek-cli` 方案中。
  4. 提供了 **应用上下文路径** 功能的支持。