<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <title>欢迎使用FDP</title>
  <!--
    @entry
    @require bootstrap
    @require app
    @require ./style/index.less
    @seajs.use ./js/index.js
  -->
</head>
<body>
  <header>
    <!-- @component fdp-header -->
  </header>
  <h1>欢迎使用神州易泰大前端开发平台(fdp)</h1>
</body>
</html>