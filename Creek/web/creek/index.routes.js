var express = require('express');
var router = express.Router();
var staticize = require('fdp-staticize');

var oIndexActions = require('./index.actions')

/*将请求静态化*/
router.get('/', staticize(oIndexActions.fnDoInit));

module.exports = router;