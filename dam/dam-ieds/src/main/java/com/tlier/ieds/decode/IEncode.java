package com.tlier.ieds.decode;

import java.util.Map;

public interface IEncode {

	/**
	 * 功能描述: <br>
	 * 编码
	 * 
	 * @param input
	 *            源
	 * @param parameters
	 *            参数
	 * @return 处理结果
	 */
	public byte[] encode(byte[] input, Map<String, Object> parameters);

	/**
	 * 功能描述: <br>
	 * 解码
	 * 
	 * @param input
	 *            源
	 * @param parameters
	 *            参数
	 * @return 处理结果
	 */
	public byte[] deEncode(byte[] input, Map<String, Object> parameters);

	/**
	 * 功能描述: <br>
	 * 生成参数
	 * 
	 * @param sender
	 *            发送人
	 * @param parameters
	 *            参数
	 */
	public void generateRelationParameters(String sender,
			Map<String, Object> parameters);

}