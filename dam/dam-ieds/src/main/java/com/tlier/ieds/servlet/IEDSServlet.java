package com.tlier.ieds.servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.tlier.app.util.ApplicationContextUtils;
import com.tlier.ieds.config.CharSetConfig;
import com.tlier.ieds.decode.IMarshall;

import com.tlier.ieds.service.LocalService;
import com.tlier.ieds.util.IEDSUtils;
import com.digitalchina.ieds.msgstruct.TiripPackage;

@SuppressWarnings("serial")
public class IEDSServlet extends HttpServlet {

	private static final Log LOG = LogFactory.getLog(IEDSServlet.class);

	@Override
	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {
		doPost(request, response);
	}

	@Override
	public void doPost(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {
		LOG.debug("请求接入，IP地址：" + request.getRemoteAddr());
		try {
			CharSetConfig charSetConfig = (CharSetConfig) ApplicationContextUtils
					.getContext().getBean("charSetConfig");
			request.setCharacterEncoding(charSetConfig.getLocalCharSet());

			byte[] xmlMessage = IEDSUtils.inputStream2byteArray(request
					.getInputStream());
			if (ArrayUtils.isEmpty(xmlMessage)) {
				LOG.debug("无法处理XML请求报文，原因：请求报文为空");
				return;
			}

			String serviceId = IEDSUtils.getServiceId(xmlMessage);
			if (StringUtils.isBlank(serviceId)) {
				LOG.debug("无法处理XML请求报文，原因：服务ID为空");
				return;
			}

			String requestXML = new String(xmlMessage,
					charSetConfig.getLocalCharSet());
			LOG.debug("XML请求报文处理开始，服务ID：" + serviceId + "，请求报文：" + requestXML);

			LocalService localService = (LocalService) ApplicationContextUtils
					.getContext().getBean("localService");
			TiripPackage tiripPackage = localService.doService(serviceId,
					xmlMessage);
			IMarshall jibxMarshall = (IMarshall) ApplicationContextUtils
					.getContext().getBean("jibxMarshall");
			byte[] buffer = jibxMarshall.marshall(TiripPackage.class,
					tiripPackage);
			String responseXML = new String(buffer,
					charSetConfig.getLocalCharSet());

			response.setContentType("application/xml");
			response.setCharacterEncoding(charSetConfig.getLocalCharSet());
			ServletOutputStream outputStream = response.getOutputStream();
			outputStream.write(responseXML.getBytes(charSetConfig
					.getLocalCharSet()));
			LOG.debug("XML请求报文处理结束，返回报文：" + responseXML);
		} catch (Exception e) {
			LOG.error("XML请求报文处理时出现异常", e);
		}
	}

}