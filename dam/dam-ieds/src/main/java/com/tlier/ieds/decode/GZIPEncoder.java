package com.tlier.ieds.decode;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Map;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;

import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class GZIPEncoder implements IEncode {

	private static final Log LOG = LogFactory.getLog(GZIPEncoder.class);

	@Override
	public byte[] encode(byte[] input, Map<String, Object> parameters) {
		if (ArrayUtils.isEmpty(input)) {
			return null;
		}
		ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
		try {
			GZIPOutputStream gzipOutputStream = new GZIPOutputStream(
					byteArrayOutputStream);
			gzipOutputStream.write(input);
			gzipOutputStream.close();
			return byteArrayOutputStream.toByteArray();
		} catch (IOException e) {
			LOG.error("使用GZIP压缩时出现异常", e);
		}
		return null;
	}

	@Override
	public byte[] deEncode(byte[] input, Map<String, Object> parameters) {
		if (ArrayUtils.isEmpty(input)) {
			return null;
		}
		ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
		try {
			GZIPInputStream gzipInputStream = new GZIPInputStream(
					new ByteArrayInputStream(input));
			byte[] buffer = new byte[1024];
			int count = 0;
			while ((count = gzipInputStream.read(buffer)) > 0) {
				byteArrayOutputStream.write(buffer, 0, count);
			}
			return byteArrayOutputStream.toByteArray();
		} catch (IOException e) {
			LOG.error("使用GZIP解压缩时出现异常", e);
		}
		return null;
	}

	@Override
	public void generateRelationParameters(String sender,
			Map<String, Object> parameters) {
	}

}