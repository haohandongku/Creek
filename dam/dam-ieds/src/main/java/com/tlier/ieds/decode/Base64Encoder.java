package com.tlier.ieds.decode;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.Map;
import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import sun.misc.BASE64Decoder;
import sun.misc.BASE64Encoder;

import com.tlier.app.util.ApplicationContextUtils;
import com.tlier.ieds.config.CharSetConfig;

public class Base64Encoder implements IEncode {

	private static final Log LOG = LogFactory.getLog(Base64Encoder.class);

	@Override
	public byte[] encode(byte[] input, Map<String, Object> parameters) {
		if (ArrayUtils.isEmpty(input)) {
			return null;
		}
		BASE64Encoder base64Encoder = new BASE64Encoder();
		try {
			CharSetConfig charSetConfig = (CharSetConfig) ApplicationContextUtils
					.getContext().getBean("charSetConfig");
			return base64Encoder.encode(input).getBytes(
					charSetConfig.getLocalCharSet());
		} catch (Exception e) {
			LOG.error("使用BASE64编码时出现异常", e);
		}
		return null;
	}

	@Override
	public byte[] deEncode(byte[] input, Map<String, Object> parameters) {
		if (ArrayUtils.isEmpty(input)) {
			return null;
		}
		BASE64Decoder base64Decoder = new BASE64Decoder();
		try {
			return base64Decoder.decodeBuffer(new ByteArrayInputStream(input));
		} catch (IOException e) {
			LOG.error("使用BASE64解码时出现异常", e);
		}
		return null;
	}

	@Override
	public void generateRelationParameters(String sender,
			Map<String, Object> parameters) {
	}

}