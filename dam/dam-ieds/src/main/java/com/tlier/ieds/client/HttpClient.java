package com.tlier.ieds.client;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.httpclient.methods.multipart.FilePart;
import org.apache.commons.httpclient.methods.multipart.MultipartRequestEntity;
import org.apache.commons.httpclient.methods.multipart.Part;
import org.apache.commons.httpclient.methods.multipart.StringPart;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.scheme.PlainSocketFactory;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.scheme.SchemeRegistry;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.conn.tsccm.ThreadSafeClientConnManager;

import com.tlier.app.data.DataObject;
import com.tlier.app.util.JacksonUtils;

@SuppressWarnings("deprecation")
public class HttpClient implements IClient {

	/** http客户端缓存池 */
	private DefaultHttpClient[] defaultHttpClients;
	/** 默认缓存池大小 */
	private int poolSize = 20;
	/** 默认最大连接数 */
	private int maxTotalConnections = 15000;
	/** 默认最大路由数 */
	private int defaultMaxPerRoute = 2000;
	/** 服务地址 */
	private String url;
	/** 请求字符集 */
	private String requestCharSet;
	/** 返回字符集 */
	private String responseCharSet;

	/**
	 * 功能描述: <br>
	 * 远程服务调用
	 * 
	 * @param requestXML
	 *            请求报文
	 * @return 返回报文
	 * @throws Exception
	 *             异常
	 */
	@Override
	public String callService(String requestXML) throws Exception {
		BufferedInputStream bufferedInputStream = null;
		org.apache.http.client.HttpClient httpClient = null;
		HttpPost httpPost = null;
		try {
			httpClient = getClient();

			httpPost = new HttpPost(url);

			StringEntity stringEntity = new StringEntity(requestXML,
					requestCharSet);
			httpPost.setEntity(stringEntity);

			HttpResponse response = httpClient.execute(httpPost);

			HttpEntity returnEntity = response.getEntity();

			if (returnEntity != null) {
				bufferedInputStream = new BufferedInputStream(
						returnEntity.getContent());
				ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
				int count;
				while ((count = bufferedInputStream.read()) != -1) {
					byteArrayOutputStream.write(count);
				}
				return new String(byteArrayOutputStream.toByteArray(),
						responseCharSet);
			}
		} catch (Exception e) {
			try {
				HttpResponse response = httpClient.execute(httpPost);

				HttpEntity returnEntity = response.getEntity();

				if (returnEntity != null) {
					bufferedInputStream = new BufferedInputStream(
							returnEntity.getContent());
					ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
					int count;
					while ((count = bufferedInputStream.read()) != -1) {
						byteArrayOutputStream.write(count);
					}
					return new String(byteArrayOutputStream.toByteArray(),
							responseCharSet);
				}
			}catch(Exception ex){
				throw new Exception("远程服务调用时出现异常", ex);
			}
		} finally {
			if (bufferedInputStream != null) {
				bufferedInputStream.close();
			}
		}
		return null;
	}

	/**
	 * 功能描述: <br>
	 * 远程文件服务调用
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
	public DataObject callFileService(String serviceName, File file,
			DataObject dataObject) throws Exception {
		try {
			Map parameter = dataObject.getMap();
			FilePart filePart = new FilePart("filedata", file);
			StringPart stringPart1 = new StringPart("serviceName", serviceName);
			StringPart stringPart2 = new StringPart("methodName", "doService");
			StringPart stringPart3 = new StringPart("requestJson",
					JacksonUtils.getJsonFromMap(parameter), requestCharSet);
			Part[] parts = { stringPart1, stringPart2, stringPart3, filePart };
			PostMethod postMethod = new PostMethod(url);
			MultipartRequestEntity mre = new MultipartRequestEntity(parts,
					postMethod.getParams());
			postMethod.setRequestEntity(mre);
			org.apache.commons.httpclient.HttpClient client = new org.apache.commons.httpclient.HttpClient();
			client.getHttpConnectionManager().getParams()
					.setConnectionTimeout(50000);
			int status = client.executeMethod(postMethod);
			String responseBody = postMethod.getResponseBodyAsString();
			Map map = new HashMap();
			map.put("STATUS", status);
			map.put("RESPONSEBODY", responseBody);
			return new DataObject(map);
		} catch (Exception e) {
			throw new Exception("远程服务调用时出现异常", e);
		}
	}

	private DefaultHttpClient getClient() {
		if (defaultHttpClients == null) {
			defaultHttpClients = new DefaultHttpClient[poolSize];
		}
		int index = getRandomIndex();
		if (defaultHttpClients[index] == null) {
			SchemeRegistry schemeRegistry = new SchemeRegistry();
			schemeRegistry.register(new Scheme("http", 80, PlainSocketFactory
					.getSocketFactory()));
			ThreadSafeClientConnManager threadSafeClientConnManager = new ThreadSafeClientConnManager(
					schemeRegistry);
			threadSafeClientConnManager.setMaxTotal(maxTotalConnections);
			threadSafeClientConnManager
					.setDefaultMaxPerRoute(defaultMaxPerRoute);
			defaultHttpClients[index] = new DefaultHttpClient(
					threadSafeClientConnManager);
		}
		return defaultHttpClients[index];
	}

	private int getRandomIndex() {
		Random random = new Random();
		return (int) (Math.floor(random.nextDouble() * this.poolSize) + 0);
	}

	public void setPoolSize(int poolSize) {
		this.poolSize = poolSize;
	}

	public void setMaxTotalConnections(int maxTotalConnections) {
		this.maxTotalConnections = maxTotalConnections;
	}

	public void setDefaultMaxPerRoute(int defaultMaxPerRoute) {
		this.defaultMaxPerRoute = defaultMaxPerRoute;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public void setRequestCharSet(String requestCharSet) {
		this.requestCharSet = requestCharSet;
	}

	public void setResponseCharSet(String responseCharSet) {
		this.responseCharSet = responseCharSet;
	}

}