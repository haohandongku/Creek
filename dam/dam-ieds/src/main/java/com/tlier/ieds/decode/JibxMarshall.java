package com.tlier.ieds.decode;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;

import org.jibx.runtime.BindingDirectory;
import org.jibx.runtime.IBindingFactory;
import org.jibx.runtime.IMarshallingContext;
import org.jibx.runtime.IUnmarshallingContext;

import com.tlier.app.util.ApplicationContextUtils;
import com.tlier.ieds.config.CharSetConfig;

public class JibxMarshall implements IMarshall {

	@Override
	@SuppressWarnings("rawtypes")
	public byte[] marshall(Class objectClass, Object object) throws Exception {
		IBindingFactory factory = BindingDirectory.getFactory(objectClass);
		IMarshallingContext context = factory.createMarshallingContext();
		ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
		CharSetConfig charSetConfig = (CharSetConfig) ApplicationContextUtils
				.getContext().getBean("charSetConfig");
		context.marshalDocument(object, charSetConfig.getLocalCharSet(), null,
				outputStream);
		outputStream.flush();
		return outputStream.toByteArray();
	}

	@Override
	@SuppressWarnings("rawtypes")
	public Object unMarshall(Class objectClass, InputStream inputStream)
			throws Exception {
		IBindingFactory factory = BindingDirectory.getFactory(objectClass);
		IUnmarshallingContext context = factory.createUnmarshallingContext();
		CharSetConfig charSetConfig = (CharSetConfig) ApplicationContextUtils
				.getContext().getBean("charSetConfig");
		return context.unmarshalDocument(inputStream,
				charSetConfig.getLocalCharSet());
	}

}