package com.tlier.ieds.config;

import java.util.LinkedHashMap;

public class ControlItem {

	/** 通道号 */
	private String channelId;
	/** 通道密码 */
	private String password;
	/** 报文控制信息 */
	private LinkedHashMap<String, String> controlConfigMap;

	public String getChannelId() {
		return channelId;
	}

	public void setChannelId(String channelId) {
		this.channelId = channelId;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public LinkedHashMap<String, String> getControlConfigMap() {
		return controlConfigMap;
	}

	public void setControlConfigMap(
			LinkedHashMap<String, String> controlConfigMap) {
		this.controlConfigMap = controlConfigMap;
	}

}