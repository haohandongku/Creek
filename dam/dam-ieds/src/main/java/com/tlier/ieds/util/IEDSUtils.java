package com.tlier.ieds.util;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;

import com.tlier.app.data.DataObject;
import com.tlier.app.util.ApplicationContextUtils;
import com.tlier.ieds.config.CharSetConfig;
import com.tlier.ieds.config.ClientConfig;
import com.tlier.ieds.config.ClientControlConfig;
import com.tlier.ieds.config.Constant;
import com.tlier.ieds.config.ControlItem;
import com.tlier.ieds.decode.IEncode;
import com.digitalchina.ieds.msgstruct.Control;
import com.digitalchina.ieds.msgstruct.Identity;
import com.digitalchina.ieds.msgstruct.Parameter;
import com.digitalchina.ieds.msgstruct.ReturnState;
import com.digitalchina.ieds.msgstruct.Sign;
import com.digitalchina.ieds.msgstruct.SubPackage;
import com.digitalchina.ieds.msgstruct.TiripPackage;

public class IEDSUtils {

	/**
	 * 功能描述: <br>
	 * 流转换成字节数组
	 * 
	 * @param inputStream
	 *            流
	 * @return 字节数组
	 * @throws IOException
	 *             异常
	 */
	public static byte[] inputStream2byteArray(InputStream inputStream)
			throws IOException {
		BufferedInputStream bufferedInputStream = new BufferedInputStream(
				inputStream);
		ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
		byte[] buffer = new byte[1024];
		int count = 0;

		while ((count = bufferedInputStream.read(buffer, 0, 1024)) > 0) {
			outputStream.write(buffer, 0, count);
		}
		return outputStream.toByteArray();
	}

	/**
	 * 功能描述: <br>
	 * 从请求报文字节数组中获取服务ID
	 * 
	 * @param xmlMessage
	 *            请求报文字节数组
	 * @return 服务ID
	 */
	public static String getServiceId(byte[] xmlMessage) {
		StringBuffer buffer = new StringBuffer();
		byte[] lowerTarget = { '<', 's', 'e', 'r', 'v', 'i', 'c', 'e', 'i',
				'd', '>' };
		byte[] upperTarget = { '<', 'S', 'E', 'R', 'V', 'I', 'C', 'E', 'I',
				'D', '>' };
		int i = 0;
		int j = 0;

		while (i < xmlMessage.length) {
			if (j == lowerTarget.length) {
				if (xmlMessage[i] == '<') {
					break;
				} else {
					if (xmlMessage[i] == 32 || xmlMessage[i] == 9) {
						continue;
					} else {
						buffer.append((char) xmlMessage[i]);
					}
				}
			} else {
				if (xmlMessage[i] == lowerTarget[j]
						|| xmlMessage[i] == upperTarget[j]) {
					j++;
				} else {
					j = 0;
				}
			}
			i++;
		}
		return buffer.toString();
	}

	/**
	 * 功能描述: <br>
	 * 解码
	 * 
	 * @param tiripPackage
	 *            请求报文
	 * @throws Exception
	 *             异常
	 */
	public static void doDecode(TiripPackage tiripPackage) throws Exception {
		List<Control> controlList = tiripPackage.getContentControl();
		if (CollectionUtils.isNotEmpty(controlList)) {
			List<IEncode> processBeans = new ArrayList<IEncode>();
			for (int i = controlList.size() - 1; i >= 0; i--) {
				Control control = controlList.get(i);
				String processBeanName = control.getType() + control.getImpl();
				IEncode processBean = (IEncode) ApplicationContextUtils
						.getContext().getBean(processBeanName);
				processBeans.add(processBean);
			}
			encode(tiripPackage, processBeans, 0);
			tiripPackage.setContentControl(null);
		}
	}

	/**
	 * 功能描述: <br>
	 * 编码
	 * 
	 * @param tiripPackage
	 *            返回报文
	 * @throws Exception
	 *             异常
	 */
	public static void doEcode(TiripPackage tiripPackage,
			LinkedHashMap<String, String> controlMap) throws Exception {
		if (controlMap != null && controlMap.size() > 0) {
			List<Control> controlList = new ArrayList<Control>();
			List<IEncode> processBeans = new ArrayList<IEncode>();
			int i = 1;
			for (Map.Entry<String, String> entry : controlMap.entrySet()) {
				Control control = new Control();
				control.setId(i);
				control.setType(entry.getKey());
				control.setImpl(entry.getValue());
				controlList.add(control);
				String processBeanName = entry.getKey() + entry.getValue();
				IEncode processBean = (IEncode) ApplicationContextUtils
						.getContext().getBean(processBeanName);
				processBeans.add(processBean);
				i++;
			}
			encode(tiripPackage, processBeans, 1);
			tiripPackage.setContentControl(controlList);
		}
	}

	/**
	 * 功能描述: <br>
	 * 生成返回状态
	 * 
	 * @param tiripPackage
	 *            返回报文
	 * @param returnCode
	 *            返回代码
	 */
	public static void generateReturnState(TiripPackage tiripPackage,
			String returnCode) {
		String returnMessage = (String) Constant.returnMessageMap
				.get(returnCode);
		ReturnState returnState = new ReturnState();
		returnState.setReturnCode(returnCode);
		returnState.setReturnMessage(returnMessage);
		tiripPackage.setReturnState(returnState);
	}

	/**
	 * 功能描述: <br>
	 * 编码/反编码
	 * 
	 * @param tiripPackage
	 *            报文
	 * @param processBeans
	 *            处理组件
	 * @param encodeModel
	 *            模式
	 * @throws Exception
	 *             异常
	 */
	private static void encode(TiripPackage tiripPackage,
			List<IEncode> processBeans, int encodeModel) throws Exception {
		List<SubPackage> businessContent = tiripPackage.getBusinessContent();
		if (CollectionUtils.isEmpty(businessContent)
				|| CollectionUtils.isEmpty(processBeans)) {
			return;
		}

		String sender = tiripPackage.getSender();
		CharSetConfig charSetConfig = (CharSetConfig) ApplicationContextUtils
				.getContext().getBean("charSetConfig");

		for (SubPackage subPackage : businessContent) {
			String content = subPackage.getContent();
			if (StringUtils.isNotBlank(content)) {
				Map<String, Object> parameters = new HashMap<String, Object>();
				List<Parameter> paramList = subPackage.getParamList();
				if (CollectionUtils.isNotEmpty(paramList)) {
					for (Parameter parameter : paramList) {
						parameters.put(parameter.getName(),
								parameter.getValue());
					}
				}

				Sign sign = tiripPackage.getSignById(subPackage.getId());
				if (sign != null) {
					parameters.put("digestValue", sign.getDigestValue());
					parameters.put("signValue", sign.getSignValue());
				}

				byte[] input = content
						.getBytes(charSetConfig.getLocalCharSet());
				for (IEncode processBean : processBeans) {
					processBean.generateRelationParameters(sender, parameters);
					if (encodeModel == 0) {
						input = processBean.deEncode(input, parameters);
					} else {
						input = processBean.encode(input, parameters);
					}
				}
				subPackage.setContent(new String(input, charSetConfig
						.getLocalCharSet()));
			}
		}
	}

	/**
	 * 构建申请报文
	 * 
	 * @param serviceId
	 *            服务ID
	 * @param dataObject
	 * @param router
	 *            路由信息
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public static TiripPackage buildTiripPackage(String serviceId,
			DataObject dataObject, Map router) throws Exception {
		ClientConfig clientConfig = (ClientConfig) ApplicationContextUtils
				.getContext().getBean("clientConfig");
		String target = clientConfig.getClientConfigMap().get(serviceId);

		ClientControlConfig clientControlConfig = (ClientControlConfig) ApplicationContextUtils
				.getContext().getBean("clientControlConfig");
		ControlItem controlItem = (ControlItem) clientControlConfig
				.getControlConfigMap().get(target);

		TiripPackage tiripPackage = new TiripPackage();
		Identity identity = new Identity();
		identity.setServiceId(serviceId);
		identity.setChannelId(controlItem.getChannelId());
		identity.setPassword(controlItem.getPassword());
		tiripPackage.setIdentity(identity);

		List<Parameter> routerSession = new ArrayList<Parameter>();
		if (router != null) {
			Iterator<Map.Entry<String, String>> it = router.entrySet()
					.iterator();
			while (it.hasNext()) {
				Map.Entry<String, String> entry = it.next();
				Parameter routerparameter = new Parameter();
				routerparameter.setName(entry.getKey());
				routerparameter.setValue(entry.getValue());
				routerSession.add(routerparameter);
			}
		}
		tiripPackage.setRouterSession(routerSession);

		SubPackage subPackage = new SubPackage();
		subPackage.setId(1);
		subPackage.setContent(dataObject.getXml());
		List<SubPackage> businessContent = new ArrayList<SubPackage>();
		businessContent.add(subPackage);
		tiripPackage.clearBusinessContent();
		tiripPackage.setBusinessContent(businessContent);

		doEcode(tiripPackage, controlItem.getControlConfigMap());
		return tiripPackage;
	}

}