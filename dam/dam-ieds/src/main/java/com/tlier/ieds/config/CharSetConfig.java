package com.tlier.ieds.config;

public class CharSetConfig {

	/** 本地字符设置 */
	private String localCharSet;

	public String getLocalCharSet() {
		return localCharSet;
	}

	public void setLocalCharSet(String localCharSet) {
		this.localCharSet = localCharSet;
	}

}