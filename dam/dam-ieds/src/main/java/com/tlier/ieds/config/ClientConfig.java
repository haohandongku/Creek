package com.tlier.ieds.config;

import java.util.LinkedHashMap;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathFactory;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class ClientConfig {

	private static final Log LOG = LogFactory.getLog(ClientConfig.class);
	private static LinkedHashMap<String, String> clientConfigMap;
	private PathMatchingResourcePatternResolver patternResolver = new PathMatchingResourcePatternResolver();
	private String clientFileLocations;

	public ClientConfig() {
		if (clientConfigMap == null) {
			clientConfigMap = new LinkedHashMap<String, String>();
		}
	}

	public void init() throws Exception {
		LOG.debug("ieds开始注册clients服务！");
		if (clientFileLocations == null) {
			return;
		}
		Resource resources[] = patternResolver
				.getResources(clientFileLocations);
		if (resources == null) {
			return;
		}
		DocumentBuilder builder = DocumentBuilderFactory.newInstance()
				.newDocumentBuilder();
		XPath xpath = XPathFactory.newInstance().newXPath();
		for (Resource resource : resources) {
			LOG.debug("加载：" + resource.getFilename());
			Document document = builder.parse(resource.getInputStream());
			NodeList nodeList = (NodeList) xpath.evaluate("/clients/client",
					document, XPathConstants.NODESET);
			for (int i = 0; i < nodeList.getLength(); i++) {
				String id = "";
				Node node = nodeList.item(i);
				Node idNode = node.getAttributes().getNamedItem("id");
				if (idNode != null) {
					id = idNode.getNodeValue().trim();
				}
				if (StringUtils.isNotBlank(id)) {
					String target = "";
					Node targetNode = node.getAttributes().getNamedItem(
							"target");
					if (targetNode != null) {
						target = targetNode.getNodeValue().trim();
					}
					clientConfigMap.put(id, target);
				} else {
					LOG.error("ieds clients的id属性不能为空！");
				}
			}
		}
		LOG.debug("ieds注册clients服务结束！");
	}

	public LinkedHashMap<String, String> getClientConfigMap() {
		return clientConfigMap;
	}

	public void setClientFileLocations(String clientFileLocations) {
		this.clientFileLocations = clientFileLocations;
	}

}