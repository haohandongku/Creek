package com.tlier.ieds.config;

import java.util.LinkedHashMap;

public class ServerControlConfig {

	private LinkedHashMap<String, ControlItem> controlConfigMap;

	public LinkedHashMap<String, ControlItem> getControlConfigMap() {
		return controlConfigMap;
	}

	public void setControlConfigMap(
			LinkedHashMap<String, ControlItem> controlConfigMap) {
		this.controlConfigMap = controlConfigMap;
	}

}