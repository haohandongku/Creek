package com.tlier.ieds.service;

import java.io.ByteArrayInputStream;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import com.tlier.app.data.DataObject;
import com.tlier.app.service.IService;
import com.tlier.app.util.ApplicationContextUtils;
import com.tlier.ieds.config.Constant;
import com.tlier.ieds.config.ControlItem;
import com.tlier.ieds.config.ServerConfig;
import com.tlier.ieds.config.ServerControlConfig;
import com.tlier.ieds.decode.IMarshall;
import com.tlier.ieds.util.IEDSUtils;
import com.digitalchina.ieds.msgstruct.TiripPackage;

public class LocalService {

	private static final Log LOG = LogFactory.getLog(LocalService.class);

	/**
	 * 功能描述: <br>
	 * 本地服务调用
	 * 
	 * @param serviceId
	 *            服务ID
	 * @param xmlMessage
	 *            请求报文
	 * @return 返回报文
	 */
	public TiripPackage doService(String serviceId, byte[] xmlMessage) {
		ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(
				xmlMessage);
		TiripPackage tiripPackage = null;
		try {
			IMarshall jibxMarshall = (IMarshall) ApplicationContextUtils
					.getContext().getBean("jibxMarshall");
			tiripPackage = (TiripPackage) jibxMarshall.unMarshall(
					TiripPackage.class, byteArrayInputStream);
			String returnCode = null;
			ServerControlConfig serverControlConfig = (ServerControlConfig) ApplicationContextUtils
					.getContext().getBean("serverControlConfig");
			ControlItem controlItem = (ControlItem) serverControlConfig
					.getControlConfigMap().get(
							tiripPackage.getIdentity().getChannelId());
			// 判断通道号和密码是否正确
			if (controlItem == null
					|| !tiripPackage.getIdentity().getPassword()
							.equals(controlItem.getPassword())) {
				returnCode = Constant.RETURN_CODE_90000003;
			} else {
				IEDSUtils.doDecode(tiripPackage);
				String requestXML = tiripPackage.getContentAt(0);
				ServerConfig serverConfig = (ServerConfig) ApplicationContextUtils
						.getContext().getBean("serverConfig");
				String serviceName = serverConfig.getServerConfigMap().get(
						serviceId);
				if (serviceName == null) {
					returnCode = Constant.RETURN_CODE_90000002;
				} else {
					IService service = (IService) ApplicationContextUtils
							.getContext().getBean(serviceName);
					DataObject dataObject = service.doService(new DataObject(
							requestXML));
					tiripPackage.clearSignData();
					tiripPackage.clearBusinessContent();
					tiripPackage.addBusinessContent(1, dataObject.getXml());
					returnCode = Constant.RETURN_CODE_00000000;
				}
			}
			IEDSUtils.generateReturnState(tiripPackage, returnCode);
			IEDSUtils.doEcode(tiripPackage, controlItem.getControlConfigMap());
		} catch (Exception e) {
			tiripPackage = new TiripPackage();
			IEDSUtils.generateReturnState(tiripPackage,
					Constant.RETURN_CODE_99999999);
			LOG.error("XML请求报文处理时出现异常", e);
		}
		return tiripPackage;
	}

}