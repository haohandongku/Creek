package com.tlier.ieds.decode;

import java.io.InputStream;

public interface IMarshall {

	/**
	 * 功能描述: <br>
	 * 请求报文实体转换成字节数组
	 * 
	 * @param objectClass
	 *            实体class
	 * @param object
	 *            实体对象
	 * @return 字节数组
	 * @throws Exception
	 *             异常
	 */
	@SuppressWarnings("rawtypes")
	public byte[] marshall(Class objectClass, Object object) throws Exception;

	/**
	 * 功能描述: <br>
	 * 请求报文转换成请求报文实体
	 * 
	 * @param objectClass
	 *            实体class
	 * @param inputStream
	 *            请求报文流
	 * @return 实体对象
	 * @throws Exception
	 *             异常
	 */
	@SuppressWarnings("rawtypes")
	public Object unMarshall(Class objectClass, InputStream inputStream)
			throws Exception;

}