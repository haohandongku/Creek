package com.tlier.ieds.proxy;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import com.tlier.app.data.DataObject;
import com.tlier.app.service.IProxy;
import com.tlier.app.service.IService;
import com.tlier.app.util.ApplicationContextUtils;
import com.tlier.ieds.client.IClient;
import com.tlier.ieds.config.CharSetConfig;
import com.tlier.ieds.config.ClientConfig;
import com.tlier.ieds.config.Constant;
import com.tlier.ieds.config.ServerConfig;
import com.tlier.ieds.decode.IMarshall;
import com.tlier.ieds.util.IEDSUtils;
import com.digitalchina.ieds.msgstruct.TiripPackage;

public class IEDSProxy implements IProxy {

	private static final Log LOG = LogFactory.getLog(IEDSProxy.class);

	/**
	 * 服务调用
	 * 
	 * @param dataObject
	 *            数据对象
	 * @param router
	 *            路由信息
	 */
	@SuppressWarnings({ "rawtypes" })
	public static DataObject doService(String serviceId, DataObject dataObject,
			Map router) {
		IProxy proxy = (IProxy) ApplicationContextUtils.getContext().getBean(
				"iedsProxy");
		return proxy.callService(serviceId, dataObject, router);
	}

	@SuppressWarnings("rawtypes")
	public static DataObject doFileService(String serviceId,
			String serviceName, File file, DataObject dataObject, Map router) {
		IProxy proxy = (IProxy) ApplicationContextUtils.getContext().getBean(
				"iedsProxy");
		return proxy.callFileService(serviceId, serviceName, file, dataObject,
				router);
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	public DataObject callService(String serviceId, DataObject dataObject,
			Map router) {
		TiripPackage tiripPackage = null;
		Map map = new HashMap();
		try {
			ClientConfig clientConfig = (ClientConfig) ApplicationContextUtils
					.getContext().getBean("clientConfig");
			String target = clientConfig.getClientConfigMap().get(serviceId);
			if (StringUtils.isBlank(target)) {				
				map.put(com.tlier.app.constant.Constant.RTN_CODE,
						Constant.RETURN_CODE_99999999);
				map.put(com.tlier.app.constant.Constant.RTN_MSG,
						"未找到服务ID对应的目的地！");
				return new DataObject(map);
			} else {
				ServerConfig serverConfig = (ServerConfig) ApplicationContextUtils
						.getContext().getBean("serverConfig");
				String serviceName = serverConfig.getServerConfigMap().get(
						serviceId);
				if (StringUtils.isNotBlank(serviceName)) {
					LOG.debug("Client优先调用本地服务，服务ID：" + serviceId);
					IService service = (IService) ApplicationContextUtils
							.getContext().getBean(serviceName);
					return service.doService(dataObject);
				}

				tiripPackage = IEDSUtils.buildTiripPackage(serviceId,
						dataObject, router);
				IMarshall jibxMarshall = (IMarshall) ApplicationContextUtils
						.getContext().getBean("jibxMarshall");
				byte[] buffer = jibxMarshall.marshall(TiripPackage.class,
						tiripPackage);
				String requestXML = new String(buffer);

				LOG.debug("Client调用服务开始，服务ID：" + serviceId + "，请求报文："
						+ requestXML);

				IClient client = (IClient) ApplicationContextUtils.getContext()
						.getBean(target + "Client");
				String responseXML = client.callService(requestXML);

				LOG.debug("Client调用服务结束，服务ID：" + serviceId + "，返回报文："
						+ responseXML);

				CharSetConfig charSetConfig = (CharSetConfig) ApplicationContextUtils
						.getContext().getBean("charSetConfig");
				ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(
						responseXML.getBytes(charSetConfig.getLocalCharSet()));
				tiripPackage = (TiripPackage) jibxMarshall.unMarshall(
						TiripPackage.class, byteArrayInputStream);

				IEDSUtils.doDecode(tiripPackage);
				
				if(!tiripPackage.getReturnState().getReturnCode().equals(Constant.RETURN_CODE_00000000)){
					//throw new Exception ("远程调用错误,错误码:"+tiripPackage.getReturnState().getReturnCode()+" "+tiripPackage.getReturnState().getReturnMessage());
					map.put(com.tlier.app.constant.Constant.RTN_CODE,
							tiripPackage.getReturnState().getReturnCode());
					map.put(com.tlier.app.constant.Constant.RTN_MSG,
							tiripPackage.getReturnState().getReturnCode() +":"+ tiripPackage.getReturnState().getReturnMessage());
				}else{
					return new DataObject(tiripPackage.getContentAt(0));
				}				
				
			}
		} catch (Exception e) {
			LOG.error("XML请求报文处理时出现异常", e);
			map.put(com.tlier.app.constant.Constant.RTN_CODE,
					Constant.RETURN_CODE_99999999);
			map.put(com.tlier.app.constant.Constant.RTN_MSG,
					e.getMessage());			
		}
		return new DataObject(map);
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	public DataObject callFileService(String serviceId, String serviceName,
			File file, DataObject dataObject, Map router) {
		try {
			ClientConfig clientConfig = (ClientConfig) ApplicationContextUtils
					.getContext().getBean("clientConfig");
			String target = clientConfig.getClientConfigMap().get(serviceId);
			if (StringUtils.isBlank(target)) {
				Map map = new HashMap();
				map.put(com.tlier.app.constant.Constant.RTN_CODE,
						Constant.RETURN_CODE_99999999);
				map.put(com.tlier.app.constant.Constant.RTN_MSG,
						"未找到服务ID对应的目的地！");
				return new DataObject(map);
			} else {
				IClient client = (IClient) ApplicationContextUtils.getContext()
						.getBean(target + "Client");
				DataObject resDataObject = client.callFileService(serviceName,
						file, dataObject);
				return resDataObject;
			}
		} catch (Exception e) {
			LOG.error("XML请求报文处理时出现异常", e);
		}
		return null;
	}

}