package com.tlier.ieds.config;

import java.util.HashMap;
import java.util.Map;

public final class Constant {

	/** 返回信息 */
	public static Map<String, String> returnMessageMap = new HashMap<String, String>();

	static {
		returnMessageMap.put(Constant.RETURN_CODE_00000000, "成功");
		returnMessageMap.put(Constant.RETURN_CODE_90000000, "数据格式非法");
		returnMessageMap.put(Constant.RETURN_CODE_90000001, "服务器忙");
		returnMessageMap.put(Constant.RETURN_CODE_90000002, "该请求不支持");
		returnMessageMap.put(Constant.RETURN_CODE_90000003, "无连接服务器权限");
		returnMessageMap.put(Constant.RETURN_CODE_90000004, "通道号和密码错误");
		returnMessageMap.put(Constant.RETURN_CODE_99999999, "未知错误");
	}

	/** 成功 */
	public static final String RETURN_CODE_00000000 = "00000000";
	/** 数据格式非法 */
	public static final String RETURN_CODE_90000000 = "90000000";
	/** 服务器忙 */
	public static final String RETURN_CODE_90000001 = "90000001";
	/** 该请求不支持 */
	public static final String RETURN_CODE_90000002 = "90000002";
	/** 无连接服务器权限 */
	public static final String RETURN_CODE_90000003 = "90000003";
	/** 通道号和密码错误 */
	public static final String RETURN_CODE_90000004 = "90000004";
	/** 未知错误 */
	public static final String RETURN_CODE_99999999 = "99999999";
	/** 公共渠道 */
	public static final String CHANNEL_ID_PUBLIC = "public";
	/** 密码 */
	public static final String PASSWORD_PUBLIC = "public";

}