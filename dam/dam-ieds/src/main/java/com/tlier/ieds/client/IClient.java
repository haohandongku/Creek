package com.tlier.ieds.client;

import java.io.File;

import com.tlier.app.data.DataObject;

public interface IClient {

	/**
	 * 功能描述: <br>
	 * 服务调用接口
	 * 
	 * @param requextXML
	 *            请求报文
	 * @return 返回报文
	 * @throws Exception
	 *             异常
	 */
	public String callService(String requextXML) throws Exception;

	/**
	 * 功能描述: <br>
	 * 文件服务调用接口
	 */
	public DataObject callFileService(String serviceName, File file,
			DataObject dataObject) throws Exception;

}