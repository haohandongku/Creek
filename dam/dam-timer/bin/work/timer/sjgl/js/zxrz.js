app.initHeader();
app.initSidebar("#nav_t2");
app.initFooter();
var pageSize = 10;
var serviceName = "timer.sjgl.SjglService";
var trgid;

$(function() {
	trgid = app.getParamsFromHref().trgid;
	
	queryZxrz(1);
});

function queryZxrz(pageNumber) {
	var param = {
		"TRGID" : trgid,
		pageSize : pageSize,
		pageNumber : pageNumber
	};
	ajax(
			serviceName,
			"queryZxrz",
			param,
			function(data) {
				var total = data.total % pageSize > 0 ? (data.total / pageSize + 1)
						: (data.total / pageSize);
				if (total > 0) {
					$('#mypager').twbsPagination({
						totalPages : total,
						onPageClick : function(event, page) {
							queryZxrz(page);
						}
					});
				}

				$('#zxrz')
						.html(
								"<li class='title'><p class='col' style='width:5%'>序号</p><p class='col' style='width:10%'>名称"
										+ "</p><p class='col' style='width:10%'>任务组名称</p><p class='col' style='width:10%'>任务名称</p>"
										+ "<p class='col' style='width:20%'>开始时间</p><p class='col' style='width:20%'>结束时间</p>"
										+ "<p class='col' style='width:10%'>处理状态</p><p class='col' style='width:15%'>处理信息</p></li>");
				$
						.each(
								data.rows,
								function(i, obj) {
									$('#zxrz')
											.append(
													"<li>"
															+ "<p class='col' style='width:5%'>"
															+ (i + 1)
															+ "</p>"
															+ "<p class='col' style='width:10%' title='" 
															+ obj.TRG_MS
															+"' >"
															+ obj.TRG_MS
															+ "</p>"
															+ "<p class='col' style='width:10%' title='" 
															+ obj.GROUP_MS
															+"' >"
															+ obj.GROUP_MS
															+ "</p>"
															+ "<p class='col' style='width:10%' title='" 
															+ obj.JOB_MS
															+"' >"
															+ obj.JOB_MS
															+ "</p>"
															+ "<p class='col' style='width:20%'>"
															+ obj.KSSJ
															+ "</p>"
															+ "<p class='col' style='width:20%'>"
															+ obj.JSSJ
															+ "</p>"
															+ "<p class='col' style='width:10%'>"
															+ obj.CLZT_MC
															+ "</p>"
															+ "<p class='col' style='width:15%' title='" 
															+ obj.CLXX
															+"' >"
															+ obj.CLXX
															+ "</p>");
								});

			});
}

function returnToSjDy(){
	window.location.href = "index.html";	
}