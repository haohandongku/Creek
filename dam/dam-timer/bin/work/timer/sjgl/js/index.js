app.initHeader();
app.initSidebar("#nav_t2");
app.initFooter();
var pageSize = 10;
var serviceName = "timer.sjgl.SjglService";
var formValiator;

$(function() {
	ajax(serviceName, "getRwzCombobox", {}, function(data) {
		$("#rwz").select2({
			data : data.rows
		});
		$("#rwz").select2("val", "");
	});
	
	ajax(serviceName, "getSjdwCombobox", {}, function(data) {
		$("#sjdw").select2({
			data : data.rows
		});
		$("#sjdw").select2("val", "");
	});
	
	
	formValiator = $("#sjForm").validate({
		rules : {
			sjms : {
				maxlength : 100
			},
			zxpd : {
				maxlength : 20
			},
			bz : {
				maxlength : 100
			}
		}
	});
	
	$("#rwz").on('select2:select',function(){
		formValiator.resetForm();
	});
	$("#sjdw").on('select2:select',function(){
		formValiator.resetForm();
	});

	querySjgl(1);
});

function querySjgl(pageNumber) {
	var param = {
		pageSize : pageSize,
		pageNumber : pageNumber
	};
	ajax(
			serviceName,
			"querySjgl",
			param,
			function(data) {
				var total = data.total % pageSize > 0 ? (data.total / pageSize + 1)
						: (data.total / pageSize);
				if (total > 0) {
					$('#mypager').twbsPagination({
						totalPages : total,
						onPageClick : function(event, page) {
							querySjgl(page);
						}
					});
				}

				$('#sjdy')
						.html(
								"<li class='title'><p class='col' style='width:7%'>序号</p><p class='col' style='width:7%'>名称"
										+ "</p><p class='col' style='width:10%'>执行频度</p><p class='col' style='width:6%'>单位</p>"
										+ "<p class='col' style='width:20%'>启动时间</p><p class='col' style='width:20%'>终止时间</p>"
										+ "<p class='col' style='width:10%'>处理状态</p><p class='col' style='width:20%'>操作</p></li>");
				$
						.each(
								data.rows,
								function(i, obj) {
									$('#sjdy')
											.append(
													"<li>"
															+ "<p class='col' style='width:7%'>"
															+ (i + 1)
															+ "</p>"
															+ "<p class='col' style='width:7%' title='" 
															+ obj.MS
															+"' >"
															+ obj.MS
															+ "</p>"
															+ "<p class='col' style='width:10%'>"
															+ obj.PD
															+ "</p>"
															+ "<p class='col' style='width:6%'>"
															+ obj.PD_MC
															+ "</p>"
															+ "<p class='col' style='width:20%'>"
															+ obj.STATIME
															+ "</p>"
															+ "<p class='col' style='width:20%'>"
															+ obj.ENDTIME
															+ "</p>"
															+ "<p class='col' style='width:10%'>"
															+ obj.CLZT_MC
															+ "</p>"
															+ "<p class='col' style='width:20%'>"
															+ convertClzt1(obj.CLZT_DM,i)
															+ "<a class='shouzhi' id='link_deleteSjgl"
															+ i
															+ "' "
															+ ">删除</a>"
															+ convertClzt2(obj.CLZT_DM,i)
															+ convertClzt3(obj.CLZT_DM,i)
															+ "</p>");
									$("#link_updateSjgl" + i).click(function() {
										updateSjgl(obj);
									});
									$("#link_deleteSjgl" + i).click(function() {
										deleteSjgl(obj);
									});
									$("#link_startSjgl" + i).click(function() {
										startSjgl(obj);
									});
									$("#link_zxRz" + i).click(function() {
										zxRz(obj);
									});
								});

			});
}

function convertClzt1(val,i){
	var html = "";
	html = html
	+ "<a id='link_updateSjgl" 
	+ i 
	+ "' class='shouzhi'" 
	+ ">";
	if("01" == val){
		html = html + "修改&nbsp;|&nbsp;" + "</a>";
	}else {
		html = "";
	}
	return html;
}

function convertClzt2(val,i){
	var html = "";
	html = html
	+ "<a id='link_startSjgl" 
	+ i 
	+ "' class='shouzhi'" 
	+ ">&nbsp;|&nbsp;";
	if("01" == val){
		html = html + "启动" + "</a>";
	}else {
		html = "";
	}
	return html;
}

function convertClzt3(val,i){
	var html = "";
	html = html
	+ "<a id='link_zxRz" 
	+ i 
	+ "' class='shouzhi'" 
	+ ">&nbsp;|&nbsp;";
	if("01" == val){
		html = "";
	}else {
		html = html + "执行日志" + "</a>";
	}
	return html;
}

function startSjgl(obj){
	var parameter = {
			"TRGID" : obj.TRGID,
			"STATIME" : obj.STATIME
		};
		ajax(serviceName, "startSjgl", parameter, function(data) {
			if (data.RtnCode == "00") {
				if (data.BizCode == "00") {
					$.dialog.alert("启动成功",function(){
						querySjgl(1);
					});					
				} else if (data.BizCode == "01") {
					$.dialog.alert("启动时间必须大于当前时间");
				}
			}else{
				$.dialog.alert("启动失败");
			}
		});
}


function updateSjgl(obj) {
	$("#rwz").prop("disabled",true);
	$("#sjms").attr("readonly", "readonly");
	$("#rwz").select2("val",obj.GROUPID);
	$("#sjms").val(obj.MS);
	$("#qdsj").val(obj.STATIME);
	$("#zzsj").val(obj.ENDTIME);
	$("#zxpd").val(obj.PD);	
	$("#sjdw").select2("val",obj.PD_DM);
	$("#bz").val(obj.BZ);
	$("#hid_sjid").val(obj.TRGID);
	formValiator.resetForm();
}

function deleteSjgl(obj) {
	  $.dialog.confirm("确定删除该时间定义吗", function() {
		var parameter = {
				"TRGID" : obj.TRGID
		};
		ajax(serviceName, "deleteSjgl", parameter, function(data) {
			if (data.RtnCode == "00") {
				$.dialog.alert("删除成功",function() {
					resetSjgl();
					querySjgl(1);
				});				
			}
		});
	}, function() {
	});
}

function zxRz(obj){
	window.location.href = "zxrz.html?trgid=" + obj.TRGID;
}

function saveSjgl() {
	var sjValid = $("#sjForm").valid();
	if (sjValid == false) {
		return;
	}
	
	var qdsj = $("#qdsj").val();
	var zzsj = $("#zzsj").val();
	if(qdsj != "" && zzsj != "" && zzsj < qdsj){
		$.dialog.alert("终止时间应大于等于启动时间");
		return;
	}

	var saveType = "";
	if ($("#hid_sjid").val() == "") {
		saveType = "add";
	} else {
		saveType = "update";
	}
	var parameter = {
			"GROUPID" : $("#rwz").val(),
			"TRGID" : $("#hid_sjid").val(),
			"MS" : $("#sjms").val(),
			"STATIME" : qdsj,
			"ENDTIME" : zzsj,
			"PD" : $("#zxpd").val(),
			"PD_DM" : $("#sjdw").val(),
			"BZ" : $("#bz").val(),
			"SAVETYPE" : saveType
	};
	ajax(serviceName, "saveSjgl", parameter, function(data) {
		if (data.RtnCode == "00") {
			if (data.BizCode == "00") {
				$.dialog.alert("保存成功",function() {
					resetSjgl();
					querySjgl(1);
				});				
			} else if (data.BizCode == "01") {
				$.dialog.alert("该时间定义名称已经存在,请选择其他时间定义名称");
			}
		}
	});
}

function resetSjgl() {
	$("#rwz").select2("val", "");
	$("#sjms").val("");
	$("#qdsj").val("");
	$("#zzsj").val("");
	$("#zxpd").val("");
	$("#sjdw").select2("val", "");
	$("#bz").val("");
	$("#hid_sjid").val("");
	$("#rwz").prop("disabled",false);
	$("#sjms").removeAttr("readonly");
}