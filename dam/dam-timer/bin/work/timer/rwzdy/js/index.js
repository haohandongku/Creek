app.initHeader();
app.initSidebar("#nav_t1");
app.initFooter();
var pageSize = 10;
var serviceName = "timer.rwzdy.RwzdyService";
var formValiator;

$(function() {
	ajax(serviceName, "getYwhjCombobox", {}, function(data) {
		$("#ywhj").select2({
			data : data.rows
		});
		$("#ywhj").select2("val", "");
	});

	formValiator = $("#rwzForm").validate({
		rules : {
			rwzms : {
				maxlength : 100
			},
			yxj : {
				maxlength : 10
			},
			bz : {
				maxlength : 100
			}
		}
	});
	
	$("#ywhj").on('select2:select',function(){
		formValiator.resetForm();
	});

	queryRwzdy(1);
});

function queryRwzdy(pageNumber) {
	var param = {
		pageSize : pageSize,
		pageNumber : pageNumber
	};
	ajax(
			serviceName,
			"queryRwzdy",
			param,
			function(data) {
				var total = data.total % pageSize > 0 ? (data.total / pageSize + 1)
						: (data.total / pageSize);
				if (total > 0) {
					$('#mypager').twbsPagination({
						totalPages : total,
						onPageClick : function(event, page) {
							queryRwzdy(page);
						}
					});
				}

				$('#rwz')
						.html(
								"<li class='title' style='margin-bottom:0;'><p class='col' style='width:10%'>序号</p><p class='col' style='width:25%'>任务组名称"
										+ "</p><p class='col' style='width:10%'>优先级</p><p class='col' style='width:10%'>业务环节</p>"
										+ "<p class='col' style='width:15%'>是否已维护任务</p><p class='col' style='width:30%'>操作</p></li>");
				$
						.each(
								data.rows,
								function(i, obj) {
									$('#rwz')
											.append(
													"<li>"
															+ "<p class='col' style='width:10%'>"
															+ (i + 1)
															+ "</p>"
															+ "<p class='col' style='width:25%' title='" 
															+ obj.MS
															+"' >"
															+ obj.MS
															+ "</p>"
															+ "<p class='col' style='width:10%'>"
															+ obj.YXJ
															+ "</p>"
															+ "<p class='col' style='width:10%'>"
															+ obj.YWHJ_MC
															+ "</p>"
															+ "<p class='col' style='width:15%'>"
															+ obj.SFWHRW
															+ "</p>"
															+ "<p class='col' style='width:30%'>"
															+ "<a class='shouzhi' id='link_updateRwzdy"
															+ i
															+ "' "
															+ ">编辑</a>"
															+ "&nbsp;|&nbsp;<a class='shouzhi' id='link_deleteRwzdy"
															+ i
															+ "' "
															+ ">删除</a>"
															+ "&nbsp;|&nbsp;<a class='shouzhi' id='link_whRw"
															+ i
															+ "' "
															+ ">维护任务</a>"
															+ "</p>");
									$("#link_updateRwzdy" + i).click(
											function() {
												updateRwzdy(obj);
											});
									$("#link_deleteRwzdy" + i).click(
											function() {
												deleteRwzdy(obj);
											});
									$("#link_whRw" + i).click(function() {
										whRw(obj);
									});
								});

			});
}

function updateRwzdy(obj) {
	$("#rwzms").attr("readonly", "readonly");
	$("#rwzms").val(obj.MS);
	$("#ywhj").select2("val",obj.YWHJ_DM);
	$("#yxj").val(obj.YXJ);
	$("#bz").val(obj.BZ);
	$("#hid_rwzid").val(obj.GROUPID);
	formValiator.resetForm();
}

function deleteRwzdy(obj) {
	$.dialog.confirm("确定删除该任务组定义吗", function() {
		var parameter = {
			"GROUPID" : obj.GROUPID
		};
		ajax(serviceName, "deleteRwzdy", parameter, function(data) {
			if (data.RtnCode == "00") {
				$.dialog.alert("删除成功",function() {
					resetRwzdy();
					queryRwzdy(1);
				});				
			}
		});
	}, function() {
	});
}

function whRw(obj) {
	window.location.href = "../rwwh/index.html?groupid=" + obj.GROUPID;
}

function saveRwzdy() {
	var rwzValid = $("#rwzForm").valid();
	if (rwzValid == false) {
		return;
	}

	var saveType = "";
	if ($("#hid_rwzid").val() == "") {
		saveType = "add";
	} else {
		saveType = "update";
	}
	var parameter = {
		"GROUPID" : $("#hid_rwzid").val(),
		"MS" : $("#rwzms").val(),
		"YWHJ_DM" : $("#ywhj").val(),
		"YXJ" : $("#yxj").val(),
		"BZ" : $("#bz").val(),
		"SAVETYPE" : saveType
	};
	ajax(serviceName, "saveRwzdy", parameter, function(data) {
		if (data.RtnCode == "00") {
			if (data.BizCode == "00") {
				$.dialog.alert("保存成功",function() {
					resetRwzdy();
					queryRwzdy(1);
				});					
			} else if (data.BizCode == "01") {
				$.dialog.alert("该任务组名称已经存在,请选择其他任务组名称");
			}

		}
	});
}

function resetRwzdy() {
	$("#rwzms").val("");
	$("#ywhj").select2("val", "");
	$("#yxj").val("");
	$("#bz").val("");
	$("#hid_rwzid").val("");
	$("#rwzms").removeAttr("readonly");
}