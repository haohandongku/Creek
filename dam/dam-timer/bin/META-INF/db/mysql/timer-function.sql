create  PROCEDURE P_SEQUENCE_TIMER_XH ( out sequenceNo  VARCHAR(20),in sequenceName VARCHAR(20) )
  comment '功能描述：标准序列计算函数
           输入参数：序列名称
           输出参数：序列值'
begin
    DECLARE ac_jdh VARCHAR(10);
    DECLARE ac_no int; 
    set ac_jdh = "XH";
    INSERT INTO SEQUENCE_TIMER_XH VALUES(NULL);
    SELECT LAST_INSERT_ID() into ac_no;
    
    set sequenceNo = CONCAT(ac_jdh,DATE_FORMAT(CURDATE(),'%y'),'9',lpad(CONCAT(ac_no),8,'0'),'000');
    
   
end;