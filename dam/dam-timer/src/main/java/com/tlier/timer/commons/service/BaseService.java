package com.tlier.timer.commons.service;

import com.tlier.timer.commons.constant.DmSequenceName;


public class BaseService extends com.tlier.app.service.BaseService {
	
	/**
	 * 获取序号
	 * 
	 * @return
	 */
	protected String getTimerXh() {
		return this.getSequence(DmSequenceName.TIMERXH);
	}

}
