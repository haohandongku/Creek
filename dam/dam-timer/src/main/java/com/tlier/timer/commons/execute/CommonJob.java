package com.tlier.timer.commons.execute;

import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.util.ReflectionUtils;

import com.tlier.app.data.DataObject;
import com.tlier.app.exception.BizRuntimeException;
import com.tlier.app.util.ApplicationContextUtils;

public class CommonJob {
	private static final Log LOG = LogFactory.getLog(CommonJob.class);

	public void execute(JobExecutionContext jobexecutioncontext)
			throws JobExecutionException {
		final Vo v = (Vo) jobexecutioncontext.getJobDetail().getJobDataMap()
				.get("vo");
		try {
			DataObject dataObject = new DataObject(str2Map(v.reqStr));
			String serviceName = v.sid;
			String methodName = v.methodName;
			Object service = ApplicationContextUtils.getContext().getBean(
					serviceName);
			Method method = ReflectionUtils.findMethod(service.getClass(),
					methodName, new Class[] { DataObject.class });
			ReflectionUtils.invokeMethod(method, service, dataObject);
		} catch (Throwable e) {
			LOG.error("任务执行出现异常：", e);
			throw new BizRuntimeException(e);
		}
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public Map str2Map(String str) {
		if (str == null || "null".equalsIgnoreCase(str)) {
			return null;
		}
		if ("".equalsIgnoreCase(str)) {
			return new HashMap();
		}
		String[] arr1 = str.split(",");
		Map map = new HashMap();
		for (int i = 0; i < arr1.length; i++) {
			String kv = arr1[i];
			String[] arr2 = kv.split("=");
			map.put(arr2[0], arr2[1]);
		}
		return map;
	}

}
