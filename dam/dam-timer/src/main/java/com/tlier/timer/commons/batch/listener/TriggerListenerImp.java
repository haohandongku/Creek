package com.tlier.timer.commons.batch.listener;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.quartz.JobExecutionContext;
import org.quartz.Trigger;
import org.quartz.TriggerListener;

public class TriggerListenerImp implements TriggerListener {
	private static final Log LOG = LogFactory.getLog(TriggerListenerImp.class);
	private String name;

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String getName() {
		return name;
	}

	@Override
	public void triggerComplete(Trigger paramTrigger,
			JobExecutionContext paramJobExecutionContext, int paramInt) {
		LOG.info("triggerName:" + paramTrigger.getName());
		LOG.info("complete");
	}

	@Override
	public void triggerFired(Trigger paramTrigger,
			JobExecutionContext paramJobExecutionContext) {
		LOG.info("fire");
		LOG.info("trigger" + paramTrigger.getGroup());
	}

	@Override
	public void triggerMisfired(Trigger paramTrigger) {
		LOG.info("triggerMisfired");
	}

	@Override
	public boolean vetoJobExecution(Trigger paramTrigger,
			JobExecutionContext paramJobExecutionContext) {
		LOG.info("vetoJobException");
		return false;
	}

}
