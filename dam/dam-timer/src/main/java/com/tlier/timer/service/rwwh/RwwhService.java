package com.tlier.timer.service.rwwh;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.CollectionUtils;

import com.tlier.app.constant.Constant;
import com.tlier.app.dao.DataWindow;
import com.tlier.app.data.DataObject;
import com.tlier.app.util.ApplicationContextUtils;
import com.tlier.timer.commons.service.BaseService;
import com.tlier.timer.service.sjgl.SjglBatchManager;

public class RwwhService extends BaseService {

	public SjglBatchManager sjglBatchManager = (SjglBatchManager) ApplicationContextUtils
	.getContext().getBean("timer.sjgl.sjglBatchManager");

	/**
	 * 查询任务定义
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public DataObject queryRwdy(DataObject dataObject) {
		Map parameter = dataObject.getMap();
		Map map = new HashMap();
		int total = DataWindow.getTotal("timer.rwwh.RwwhService_countRwdy",
				parameter);
		map.put("total", total);
		DataWindow dataWindow = DataWindow.query(
				"timer.rwwh.RwwhService_queryRwdy", parameter,
				getPageNumber(parameter), getPageSize(parameter));
		map.put("rows", dataWindow.getList());
		return new DataObject(map);
	}

	/**
	 * 保存任务定义
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public DataObject saveRwdy(DataObject dataObject) {
		Map result = new HashMap();
		Map parameter = dataObject.getMap();
		String saveType = (String) parameter.get("SAVETYPE");
		if ("add".equals(saveType)) {
			int n = DataWindow.getTotal("timer.rwwh.RwwhService_countRwmc",
					parameter);
			if (n > 0) {
				result.put(Constant.BIZ_CODE, "01");
			} else {
				String jobid = getTimerXh();
				parameter.put("JOBID", jobid);
				DataWindow.insert("timer.rwwh.RwwhService_insertRwdy",
						parameter);
				result.put(Constant.BIZ_CODE, "00");
			}
		} else if ("update".equals(saveType)) {
			DataWindow.update("timer.rwwh.RwwhService_updateRwdy", parameter);
			result.put(Constant.BIZ_CODE, "00");
		}
		return new DataObject(result);
	}

	/**
	 * 删除任务定义
	 */
	@SuppressWarnings({ "rawtypes" })
	public DataObject deleteRwdy(DataObject dataObject) {
		Map result = new HashMap();
		Map parameter = dataObject.getMap();
		DataWindow dataWindow = DataWindow.query(
				"timer.rwwh.RwwhService_queryJobTriggerByJobid", parameter);
		List list = dataWindow.getList();
		if (CollectionUtils.isNotEmpty(list)) {
			for (Object obj : list) {
				Map temp = (Map) obj;
				String tJobid = (String) temp.get("JOBID");
				String tGroupid = (String) temp.get("GROUPID");
				String tTrgid = (String) temp.get("TRGID");
				sjglBatchManager.delete(tJobid, tGroupid, tTrgid);
			}
		}
		// 删除jy_pcl_job_schedule中等于jobid相关的数据
		DataWindow.delete("timer.rwwh.RwwhService_deleteJyPclJobSchByJobid",
				parameter);
		// 删除jy_pcl_job中等于jobid相关的数据
		DataWindow.delete("timer.rwwh.RwwhService_deleteJyPclJobByJobid",
				parameter);
		return new DataObject(result);
	}

}
