package com.tlier.timer.commons.batch;

import java.lang.reflect.Method;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.context.ApplicationContext;
import org.springframework.scheduling.quartz.QuartzJobBean;

public class QuartzJobBeanSub extends QuartzJobBean {

    private static final Log LOG = LogFactory.getLog(QuartzJobBeanSub.class);

    private String targetObject;
    private String targetMethod;
    private ApplicationContext ctx;

    protected void executeInternal(JobExecutionContext context) throws JobExecutionException {
        try {
            LOG.info("execute [" + targetObject + "] at once>>>>>>");
            Object otargetObject = ctx.getBean(targetObject);
            Method m = null;
            try {
                m = otargetObject.getClass().getMethod(targetMethod, new Class[] {});

                m.invoke(otargetObject, new Object[] {});
            } catch (SecurityException e) {
                LOG.error(e);
            } catch (NoSuchMethodException e) {
                LOG.error(e);
            }
        } catch (Throwable e) {
            throw new JobExecutionException(e);
        }
    }

    public void setApplicationContext(ApplicationContext applicationContext) {
        this.ctx = applicationContext;
    }

    public void setTargetObject(String targetObject) {
        this.targetObject = targetObject;
    }

    public void setTargetMethod(String targetMethod) {
        this.targetMethod = targetMethod;
    }

}
