package com.tlier.timer.commons.batch;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;

public class BatchManager {

	private static final Log LOG = LogFactory.getLog(BatchManager.class);
    private Scheduler scheduler;

    public void setScheduler(Scheduler scheduler) {
        this.scheduler = scheduler;
    }

    @SuppressWarnings({ "rawtypes", "unchecked" })
    public void init() {
        try {
        	LOG.info(scheduler.getSchedulerName() + " initializing... ");

            Map jobs = new HashMap();

            String[] triggerGroups = scheduler.getTriggerGroupNames();
            for (String s : triggerGroups) {
            	LOG.info("组 " + s);
                String[] jobNames = scheduler.getJobNames(s);
                for (String x : jobNames) {
                	LOG.info("加载任务：  " + x);
                    jobs.put(x, x);
                }
            }
        } catch (SchedulerException e) {
        	LOG.error(e);
        }
    }

}
