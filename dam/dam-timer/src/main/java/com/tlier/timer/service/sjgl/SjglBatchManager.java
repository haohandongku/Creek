package com.tlier.timer.service.sjgl;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.quartz.CronTrigger;
import org.quartz.JobDetail;
import org.quartz.Scheduler;
import org.quartz.SimpleTrigger;

import com.tlier.app.dao.DataWindow;
import com.tlier.timer.commons.constant.SrvConstants;
import com.tlier.timer.commons.execute.JobWithState;
import com.tlier.timer.commons.execute.JobWithoutState;
import com.tlier.timer.commons.execute.Vo;
import com.tlier.timer.commons.service.BaseService;

public class SjglBatchManager extends BaseService {

	private static final Log LOG = LogFactory.getLog(SjglBatchManager.class);
	private Scheduler scheduler;

	public SjglBatchManager() {

	}

	public void setScheduler(Scheduler scheduler) {
		this.scheduler = scheduler;
	}

	public Scheduler getScheduler() {
		return scheduler;
	}

	/**
	 * 功能描述: <br>
	 * 调度任务的启动(可以启动多个)
	 */
	@SuppressWarnings({ "rawtypes" })
	public boolean start(Map map) throws Exception {
		String trgid = (String) map.get("TRGID");
		Map pclTrigger = getTriggerById(trgid);// 根据trgid获取相应的调度信息
		String[] groups = getGroupsByTrgid(trgid);// 根据trgid获取所有的任务组号
		if (groups != null) {
			for (int j = 0; j < groups.length; j++) {
				List jobs = getJobByGroupId(groups[j]);// 根据任务组号获取所有的任务号
				if (jobs != null) {
					for (int k = 0; k < jobs.size(); k++) {
						SimpleDateFormat sdf = new SimpleDateFormat(
								SrvConstants.STR_YYYY_MM_DD_HH_MM_SS);
						boolean f = true;
						String pddm = sNull((String) pclTrigger.get("PD_DM"))
								.trim();
						long pd = Long.parseLong(sNull(
								(String) pclTrigger.get("PD")).trim());
						long reInt = 0;
						if (pddm.equals(SrvConstants.PD_MINUTE)) {
							reInt = pd * SrvConstants.CONST_SZ60 * 1000L;
							f = false;
						} else if (pddm.equals(SrvConstants.PD_HOUR)) {
							reInt = pd * SrvConstants.CONST_SZ60
									* SrvConstants.CONST_SZ60 * 1000L;
							f = false;
						} else if (pddm.equals(SrvConstants.PD_DAY)) {
							reInt = pd * SrvConstants.CONST_SZ24
									* SrvConstants.CONST_SZ60
									* SrvConstants.CONST_SZ60 * 1000L;
							f = false;
						} else if (pddm.equals(SrvConstants.PD_WEEK)) {
							reInt = pd * SrvConstants.CONST_SZ7
									* SrvConstants.CONST_SZ24
									* SrvConstants.CONST_SZ60
									* SrvConstants.CONST_SZ60 * 1000L;
							f = false;
						}
						String[] ececute = sNull(
								(String) ((Map) jobs.get(k)).get("ECECUTE"))
								.split(SrvConstants.STR_SPACE);
						Vo vo = new Vo();
						vo.sid = ececute[0];
						vo.methodName = ececute[1];
						if (ececute.length > 2) {
							vo.reqStr = ececute[2];
						} else {
							vo.reqStr = "";
						}
						String jobstate = sNull(
								(String) ((Map) jobs.get(k)).get("JOBSTATE"))
								.trim();
						JobDetail jobdetail = null;
						// JOB+任务Id+调度id
						StringBuffer sb1 = new StringBuffer();
						sb1.append(SrvConstants.STR_JOB)
								.append(SrvConstants.STR_SHORTLINE)
								.append((String) ((Map) jobs.get(k))
										.get("JOBID"))
								.append(SrvConstants.STR_SHORTLINE)
								.append(trgid);
						if ("Y".equals(jobstate)) {
							jobdetail = new JobDetail(sb1.toString(),
									groups[j], JobWithState.class);
						} else {
							jobdetail = new JobDetail(sb1.toString(),
									groups[j], JobWithoutState.class);
						}
						jobdetail.getJobDataMap().put("vo", vo);
						for (int l = 1; l < ececute.length; l++) {
							jobdetail.getJobDataMap().put(String.valueOf(l),
									ececute[l]);
						}
						jobdetail.setRequestsRecovery(true);
						// TRI+调度Id+任务id
						StringBuffer sb2 = new StringBuffer();
						sb2.append(SrvConstants.STR_TRI)
								.append(SrvConstants.STR_SHORTLINE)
								.append(trgid)
								.append(SrvConstants.STR_SHORTLINE)
								.append((String) ((Map) jobs.get(k))
										.get("JOBID"));
						// 如果是频度是月或是年使用crontrigger
						if (f) {
							CronTrigger cronTrigger = new CronTrigger();
							cronTrigger
									.setStartTime(sdf.parse(sNull(
											(String) pclTrigger.get("STATIME"))
											.trim()));
							String end = sNull(
									(String) pclTrigger.get("ENDTIME")).trim();
							if (end.equals("")) {
								cronTrigger.setEndTime(null);
							} else {
								cronTrigger.setEndTime(sdf.parse(end.trim()));
							}
							String cron = getCronByMonthAndYear(
									pddm,
									sNull((String) pclTrigger.get("PD")).trim(),
									sNull((String) pclTrigger.get("STATIME"))
											.trim(), end);
							cronTrigger.setCronExpression(cron);
							cronTrigger.setGroup(groups[j]);

							cronTrigger.setName(sb2.toString());
							scheduler.scheduleJob(jobdetail, cronTrigger);
						} else {
							SimpleTrigger simpleTrigger = new SimpleTrigger();
							simpleTrigger
									.setStartTime(sdf.parse(sNull(
											(String) pclTrigger.get("STATIME"))
											.trim()));
							String end = sNull(
									(String) pclTrigger.get("ENDTIME")).trim();
							if (end.equals("")) {
								simpleTrigger.setEndTime(null);
							} else {
								simpleTrigger.setEndTime(sdf.parse(end.trim()));
							}
							simpleTrigger
									.setRepeatCount(SimpleTrigger.REPEAT_INDEFINITELY);
							simpleTrigger.setRepeatInterval(reInt);
							simpleTrigger.setGroup(groups[j]);
							simpleTrigger.setName(sb2.toString());
							scheduler.scheduleJob(jobdetail, simpleTrigger);
						}
					}
				}
			}
			// 启动调度器的同时修改调度器的状态
			updateClztToTrigger(trgid, SrvConstants.CLZT_RUNNING);
		}
		return true;
	}

	/**
	 * 功能描述: <br>
	 * 通过trgid获取相应的调度信息
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	private Map getTriggerById(String trgid) {
		Map pclTrigger = null;
		Map reqmap = new HashMap();
		reqmap.put("TRGID", trgid);
		DataWindow dataWindow = DataWindow.query(
				"timer.sjgl.SjglService_queryTriggerById", reqmap);
		List list = dataWindow.getList();
		if (CollectionUtils.isNotEmpty(list)) {
			pclTrigger = (Map) list.get(0);
		}
		return pclTrigger;
	}

	/**
	 * 功能描述: <br>
	 * 根据trgid查询所有与其有关的groupid
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	private String[] getGroupsByTrgid(String trgid) {
		String[] groups = null;
		Map reqmap = new HashMap();
		reqmap.put("TRGID", trgid);
		DataWindow dataWindow = DataWindow.query(
				"timer.sjgl.SjglService_queryGroupsByTrgid", reqmap);
		List<Object> list = dataWindow.getList();
		if (CollectionUtils.isNotEmpty(list)) {
			int n = list.size();
			groups = list.toArray(new String[n]);
		}
		return groups;
	}

	/**
	 * 功能描述: <br>
	 * 根据groupid查询所有任务job
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	private List getJobByGroupId(String groupid) {
		Map reqmap = new HashMap();
		reqmap.put("GROUPID", groupid);
		DataWindow dataWindow = DataWindow.query(
				"timer.sjgl.SjglService_queryJobByGroupId", reqmap);
		List<Object> list = dataWindow.getList();
		return list;
	}

	/**
	 * 功能描述: <br>
	 * 根据频度代码、频度、启动时间、终止时间获取cron
	 */
	private String getCronByMonthAndYear(String pddm, String pd, String start,
			String end) {
		String cron = "";
		try {
			DateFormat df = new SimpleDateFormat(
					SrvConstants.STR_YYYY_MM_DD_HH_MM_SS);
			Date date = df.parse(start);
			Calendar cal = Calendar.getInstance();
			cal.setTimeInMillis(date.getTime());
			if (pddm.equals(SrvConstants.PD_MONTH)) {
				cron = "0 " + cal.get(Calendar.MINUTE) + SrvConstants.STR_SPACE
						+ cal.get(Calendar.HOUR_OF_DAY)
						+ SrvConstants.STR_SPACE
						+ cal.get(Calendar.DAY_OF_MONTH)
						+ SrvConstants.STR_SPACE
						+ (cal.get(Calendar.MONTH) + 1) + "/" + pd + " ? "
						+ " * ";
			} else if (pddm.equals(SrvConstants.PD_YEAR)) {
				cron = "0 " + cal.get(Calendar.MINUTE) + SrvConstants.STR_SPACE
						+ cal.get(Calendar.HOUR_OF_DAY)
						+ SrvConstants.STR_SPACE
						+ cal.get(Calendar.DAY_OF_MONTH)
						+ SrvConstants.STR_SPACE
						+ (cal.get(Calendar.MONTH) + 1) + " ? "
						+ (cal.get(Calendar.YEAR)) + "/" + pd;
			}
		} catch (ParseException e) {
			LOG.error("cron转换出错!", e);
		}
		return cron;
	}

	/**
	 * 功能描述: <br>
	 * 修改调度器的状态
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public void updateClztToTrigger(String trgid, String clztDm) {
		Map reqmap = new HashMap();
		reqmap.put("TRGID", trgid);
		reqmap.put("CLZT_DM", clztDm);
		DataWindow.update("timer.sjgl.SjglService_updatePclTrigger", reqmap);
	}

	/**
	 * 功能描述: <br>
	 * 任务运行时添加相应的监控信息
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public void saveJobSchedule(String jobid, String trgid, String clztDm,
			String clxx, Date kssj) {
		Calendar c = Calendar.getInstance();
		c.setTime(kssj);
		SimpleDateFormat sj = new SimpleDateFormat(
				SrvConstants.STR_YYYY_MM_DD_HH_MM_SS);
		String ksrq = sj.format(c.getTime());
		Map reqmap = new HashMap();
		String scid = getTimerXh();
		reqmap.put("SCID", scid);
		reqmap.put("TRGID", trgid);
		reqmap.put("JOBID", jobid);
		reqmap.put("CLZT_DM", clztDm);
		reqmap.put("CLXX", clxx);
		reqmap.put("KSSJ", ksrq);
		DataWindow.insert("timer.sjgl.SjglService_insertToJobSch", reqmap);
	}

	/**
	 * 功能描述: <br>
	 * 修改调度监控表中的任务的处理状态
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public void updateToJobSch(String jobid, String trgid, String clztDm,
			String clxx, Date kssj, Date jssj) {
		Calendar c = Calendar.getInstance();
		c.setTime(kssj);
		SimpleDateFormat sj = new SimpleDateFormat(
				SrvConstants.STR_YYYY_MM_DD_HH_MM_SS);
		String ksrq = sj.format(c.getTime());
		c.setTime(jssj);
		SimpleDateFormat sj2 = new SimpleDateFormat(
				SrvConstants.STR_YYYY_MM_DD_HH_MM_SS);
		String jsrq = sj2.format(c.getTime());
		Map reqmap = new HashMap();
		reqmap.put("TRGID", trgid);
		reqmap.put("JOBID", jobid);
		reqmap.put("CLZT_DM", clztDm);
		reqmap.put("CLXX", clxx);
		reqmap.put("KSSJ", ksrq);
		reqmap.put("JSSJ", jsrq);
		DataWindow.update("timer.sjgl.SjglService_updateToJobSch", reqmap);
	}

	/**
	 * 功能描述: <br>
	 * 停止job
	 */
	public void pausedJob(String jobid, String jobGroup, String trgid) {
		try {
			String jobName = new StringBuffer().append(SrvConstants.STR_JOB)
					.append(SrvConstants.STR_SHORTLINE).append(jobid)
					.append(SrvConstants.STR_SHORTLINE).append(trgid)
					.toString();
			String[] groupNames = scheduler.getJobGroupNames();
			for (String gn : groupNames) {
				if (gn.equals(jobGroup)) {
					String[] jobNames = scheduler.getJobNames(gn);
					for (String jn : jobNames) {
						if (jn.equals(jobName)) {
							scheduler.pauseJob(jobName, jobGroup);
						}
					}
				}
			}
		} catch (Throwable e) {
			LOG.error("停止任务出错!", e);
		}
	}

	/**
	 * 功能描述: <br>
	 * 暂停所有任务
	 */
	public void pauseAll() {
		try {
			scheduler.pauseAll();
		} catch (Throwable e) {
			LOG.error("暂停所有任务出错!", e);
		}
	}

	/**
	 * 功能描述: <br>
	 * 暂停定时器中任务
	 */
	public void pause(String jobid, String jobGroup, String trgid) {
		try {
			String jobName = new StringBuffer().append(SrvConstants.STR_JOB)
					.append(SrvConstants.STR_SHORTLINE).append(jobid)
					.append(SrvConstants.STR_SHORTLINE).append(trgid)
					.toString();
			String[] groupNames = scheduler.getJobGroupNames();
			for (String gn : groupNames) {
				if (gn.equals(jobGroup)) {
					String[] jobNames = scheduler.getJobNames(gn);
					for (String jn : jobNames) {
						if (jn.equals(jobName)) {
							scheduler.pauseJob(jobName, jobGroup);
						}
					}
				}
			}
		} catch (Throwable e) {
			LOG.error("暂停定时器中任务出错!", e);
		}
	}

	/**
	 * 功能描述: <br>
	 * 重启所有的任务
	 */
	public void resumeAll() {
		try {
			scheduler.resumeAll();
		} catch (Throwable e) {
			LOG.error("重启所有任务出错!", e);
		}
	}

	/**
	 * 功能描述: <br>
	 * 重启定时器中任务
	 */
	public void resume(String jobid, String jobGroup, String trgid) {
		try {
			String jobName = new StringBuffer().append(SrvConstants.STR_JOB)
					.append(SrvConstants.STR_SHORTLINE).append(jobid)
					.append(SrvConstants.STR_SHORTLINE).append(trgid)
					.toString();
			String[] groupNames = scheduler.getJobGroupNames();
			for (String gn : groupNames) {
				if (gn.equals(jobGroup)) {
					String[] jobNames = scheduler.getJobNames(gn);
					for (String jn : jobNames) {
						if (jn.equals(jobName)) {
							scheduler.resumeJob(jobName, jobGroup);
						}
					}
				}
			}
		} catch (Throwable e) {
			LOG.error("重启定时器中任务出错!", e);
		}
	}

	/**
	 * 功能描述: <br>
	 * 删除定时器中任务
	 */
	public void delete(String jobid, String jobGroup, String trgid) {
		try {
			String jobName = new StringBuffer().append(SrvConstants.STR_JOB)
					.append(SrvConstants.STR_SHORTLINE).append(jobid)
					.append(SrvConstants.STR_SHORTLINE).append(trgid)
					.toString();
			String[] groupNames = scheduler.getJobGroupNames();
			for (String gn : groupNames) {
				if (gn.equals(jobGroup)) {
					String[] jobNames = scheduler.getJobNames(gn);
					for (String jn : jobNames) {
						if (jn.equals(jobName)) {
							scheduler.deleteJob(jobName, jobGroup);
						}
					}
				}
			}
		} catch (Throwable e) {
			LOG.error("删除定时器中任务出错!", e);
		}
	}

	/**
	 * 功能描述: <br>
	 * 停启运的任务的注册
	 * 
	 * @param name
	 *            停启运任务的名称
	 * @param date
	 *            停启运的时间
	 * @param opt
	 *            停启运定时任务类
	 */
	@SuppressWarnings("rawtypes")
	public synchronized boolean doTqy(String name, String date, Class opt) {
		boolean f = true;
		try {
			DateFormat df = new SimpleDateFormat("yyyyMMddHHmmss");
			Date tmp = df.parse(date);
			Calendar cal = Calendar.getInstance();
			cal.setTimeInMillis(tmp.getTime());
			String cron = "0 " + cal.get(Calendar.MINUTE)
					+ SrvConstants.STR_SPACE + cal.get(Calendar.HOUR_OF_DAY)
					+ SrvConstants.STR_SPACE + cal.get(Calendar.DAY_OF_MONTH)
					+ SrvConstants.STR_SPACE + (cal.get(Calendar.MONTH) + 1)
					+ " ? " + cal.get(Calendar.YEAR);
			delete(name, Scheduler.DEFAULT_GROUP, "");// 删除已存在的
			CronTrigger cronTrigger = new CronTrigger(name,
					Scheduler.DEFAULT_GROUP, cron);
			JobDetail jobDetail = new JobDetail(name, Scheduler.DEFAULT_GROUP,
					opt);
			scheduler.scheduleJob(jobDetail, cronTrigger);
		} catch (Throwable e) {
			f = false;
			LOG.error("停启运定时任务创建失败!", e);
		}
		return f;
	}

	public String sNull(String src) {
		if (src == null) {
			return "";
		} else {
			return src;
		}
	}
}
