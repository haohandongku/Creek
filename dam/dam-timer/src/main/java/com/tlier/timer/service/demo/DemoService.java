package com.tlier.timer.service.demo;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.tlier.app.data.DataObject;

public class DemoService {

	private static final Log LOG = LogFactory.getLog(DemoService.class);

	@SuppressWarnings("rawtypes")
	public DataObject execute(DataObject dataObject) {
		Map map = dataObject.getMap();
		Calendar c = Calendar.getInstance();
		c.setTime(new Date());
		SimpleDateFormat sj = new SimpleDateFormat("HH:mm:ss");
		String dqrq = sj.format(c.getTime());
		LOG.info("=========================任务被执行=========================");
		LOG.info("任务执行体的参数个数： " + map.size());
		LOG.info("任务被执行的时间 " + dqrq);
		return new DataObject();
	}

}