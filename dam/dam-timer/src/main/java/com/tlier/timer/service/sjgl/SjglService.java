package com.tlier.timer.service.sjgl;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.CollectionUtils;
import org.quartz.Scheduler;

import com.tlier.app.constant.Constant;
import com.tlier.app.dao.DataWindow;
import com.tlier.app.data.DataObject;
import com.tlier.app.util.ApplicationContextUtils;
import com.tlier.timer.commons.constant.SrvConstants;
import com.tlier.timer.commons.service.BaseService;

public class SjglService extends BaseService {

	public SjglBatchManager sjglBatchManager = (SjglBatchManager) ApplicationContextUtils
			.getContext().getBean("timer.sjgl.sjglBatchManager");

	/**
	 * 初始化任务组下拉选择框
	 */
	public DataObject getRwzCombobox(DataObject dataObject) {
		return this.initCombobox(dataObject,
				"timer.sjgl.SjglService_getRwzCombobox");
	}

	/**
	 * 初始化时间单位下拉选择框
	 */
	public DataObject getSjdwCombobox(DataObject dataObject) {
		return this.initCombobox(dataObject,
				"timer.sjgl.SjglService_getSjdwCombobox");
	}

	/**
	 * 查询时间定义
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public DataObject querySjgl(DataObject dataObject) throws Throwable {
		Scheduler scheduler = sjglBatchManager.getScheduler();
		String[] triggerGroups = scheduler.getTriggerGroupNames();
		for (String s : triggerGroups) {
			String[] triggerNames = scheduler.getTriggerNames(s);
			for (String t : triggerNames) {
				int state = scheduler.getTriggerState(t, s);
				if (state == 2) {
					String[] triNameArray = t.split(SrvConstants.STR_SHORTLINE);
					String trgid = triNameArray[1];
					sjglBatchManager.updateClztToTrigger(trgid,
							SrvConstants.CLZT_FINISHED);
				}
			}
		}
		Map parameter = dataObject.getMap();
		Map map = new HashMap();
		int total = DataWindow.getTotal("timer.sjgl.SjglService_countSjgl",
				parameter);
		map.put("total", total);
		DataWindow dataWindow = DataWindow.query(
				"timer.sjgl.SjglService_querySjgl", parameter,
				getPageNumber(parameter), getPageSize(parameter));
		map.put("rows", dataWindow.getList());
		return new DataObject(map);
	}

	/**
	 * 保存时间定义
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public DataObject saveSjgl(DataObject dataObject) {
		Map result = new HashMap();
		Map parameter = dataObject.getMap();
		String saveType = (String) parameter.get("SAVETYPE");
		if ("add".equals(saveType)) {
			int n = DataWindow.getTotal("timer.sjgl.SjglService_countSjdymc",
					parameter);
			if (n > 0) {
				result.put(Constant.BIZ_CODE, "01");
			} else {
				String trgid = getTimerXh();
				parameter.put("TRGID", trgid);
				parameter.put("CLZT_DM", SrvConstants.CLZT_READY);
				DataWindow.insert("timer.sjgl.SjglService_insertJyPclTrigger",
						parameter);
				DataWindow.insert(
						"timer.sjgl.SjglService_insertJyPclTriggerGroup",
						parameter);
				result.put(Constant.BIZ_CODE, "00");
			}
		} else if ("update".equals(saveType)) {
			DataWindow.update("timer.sjgl.SjglService_updateJyPclTrigger",
					parameter);
			result.put(Constant.BIZ_CODE, "00");
		}
		return new DataObject(result);
	}

	/**
	 * 删除任务组定义
	 */
	@SuppressWarnings({ "rawtypes" })
	public DataObject deleteSjgl(DataObject dataObject) {
		Map result = new HashMap();
		Map parameter = dataObject.getMap();
		DataWindow dataWindow = DataWindow.query(
				"timer.sjgl.SjglService_queryJobTriggerByTrgid", parameter);
		List list = dataWindow.getList();
		if (CollectionUtils.isNotEmpty(list)) {
			for (Object obj : list) {
				Map temp = (Map) obj;
				String tJobid = (String) temp.get("JOBID");
				String tGroupid = (String) temp.get("GROUPID");
				String tTrgid = (String) temp.get("TRGID");
				sjglBatchManager.delete(tJobid, tGroupid, tTrgid);
			}
		}
		// 删除jy_pcl_job_schedule中等于trgid相关的数据
		DataWindow.delete("timer.sjgl.SjglService_deleteJyPclJobSchByTrgid",
				parameter);
		// 删除jy_pcl_trigger_group中等于trgid相关的数据
		DataWindow.delete(
				"timer.sjgl.SjglService_deleteJyPclTriggerGroupByTrgid",
				parameter);
		// 删除jy_pcl_trigger中等于trgid相关的数据
		DataWindow.delete("timer.sjgl.SjglService_deleteJyPclTriggerByTrgid",
				parameter);
		return new DataObject(result);
	}

	/**
	 * 启动时间管理
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public DataObject startSjgl(DataObject dataObject) throws Throwable {
		Map result = new HashMap();
		Map parameter = dataObject.getMap();
		String qdsj = (String) parameter.get("STATIME");
		int num = compareDate(qdsj, getNowTime(false));
		if (num != 1) {
			result.put(Constant.BIZ_CODE, "01");
		} else {
			sjglBatchManager.start(parameter);
			result.put(Constant.BIZ_CODE, "00");
		}
		return new DataObject(result);
	}

	public String getNowTime(boolean time) {
		Date now = new Date();
		String format = "";
		if (time) {
			format = "yyyy-MM-dd ";
		} else {
			format = "yyyy-MM-dd HH:mm:ss";
		}
		SimpleDateFormat sdf = new SimpleDateFormat(format);
		String nowtime = sdf.format(now);
		return nowtime;
	}

	public int compareDate(String date1, String date2) throws Throwable {
		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Date dt1 = df.parse(date1);
		Date dt2 = df.parse(date2);
		if (dt1.getTime() > dt2.getTime()) {
			return 1;
		} else if (dt1.getTime() < dt2.getTime()) {
			return -1;
		} else {
			return 0;
		}
	}

	/**
	 * 查询执行日志
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public DataObject queryZxrz(DataObject dataObject) throws Throwable {
		Map parameter = dataObject.getMap();
		Map map = new HashMap();
		int total = DataWindow.getTotal("timer.sjgl.SjglService_countZxrz",
				parameter);
		map.put("total", total);
		DataWindow dataWindow = DataWindow.query(
				"timer.sjgl.SjglService_queryZxrz", parameter,
				getPageNumber(parameter), getPageSize(parameter));
		map.put("rows", dataWindow.getList());
		return new DataObject(map);
	}

}
