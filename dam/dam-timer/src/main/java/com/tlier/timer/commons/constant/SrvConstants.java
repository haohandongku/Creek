package com.tlier.timer.commons.constant;

public class SrvConstants {   
    
    public static final int CONST_SZ7 = 7;
    public static final int CONST_SZ24 = 24;
    public static final int CONST_SZ60 = 60;
    
    public static final String CLZT_READY = "01";
    public static final String CLZT_RUNNING = "02";
    public static final String CLZT_FINISHED = "06";
    
    public static final String PD_MINUTE = "01";
    public static final String PD_HOUR = "02";
    public static final String PD_DAY = "03";
    public static final String PD_WEEK = "04";
    public static final String PD_MONTH = "05";
    public static final String PD_YEAR = "06";
    
    public static final String STR_YYYY_MM_DD_HH_MM_SS = "yyyy-MM-dd HH:mm:ss";
    public static final String STR_SHORTLINE = "-"; 
    public static final String STR_SPACE = " "; 
    public static final String STR_JOB = "JOB";
    public static final String STR_TRI = "TRI";
    

}