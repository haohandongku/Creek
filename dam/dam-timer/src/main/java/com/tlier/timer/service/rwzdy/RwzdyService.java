package com.tlier.timer.service.rwzdy;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.CollectionUtils;

import com.tlier.app.constant.Constant;
import com.tlier.app.dao.DataWindow;
import com.tlier.app.data.DataObject;
import com.tlier.app.util.ApplicationContextUtils;
import com.tlier.timer.commons.service.BaseService;
import com.tlier.timer.service.sjgl.SjglBatchManager;

public class RwzdyService extends BaseService {

	public SjglBatchManager sjglBatchManager = (SjglBatchManager) ApplicationContextUtils
			.getContext().getBean("timer.sjgl.sjglBatchManager");

	/**
	 * 初始化业务环节下拉选择框
	 */
	public DataObject getYwhjCombobox(DataObject dataObject) {
		return this.initCombobox(dataObject,
				"timer.rwzdy.RwzdyService_getYwhjCombobox");
	}

	/**
	 * 查询任务组定义
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public DataObject queryRwzdy(DataObject dataObject) {
		Map parameter = dataObject.getMap();
		Map map = new HashMap();
		int total = DataWindow.getTotal("timer.rwzdy.RwzdyService_countRwzdy",
				parameter);
		map.put("total", total);
		DataWindow dataWindow = DataWindow.query(
				"timer.rwzdy.RwzdyService_queryRwzdy", parameter,
				getPageNumber(parameter), getPageSize(parameter));
		List list = dataWindow.getList();
		List resList = new ArrayList();
		if (CollectionUtils.isNotEmpty(list)) {
			for (Object object : list) {
				Map temp = (Map) object;
				Map m = new HashMap();
				m.put("GROUPID", String.valueOf(temp.get("GROUPID")));
				int total2 = DataWindow.getTotal(
						"timer.rwzdy.RwzdyService_countRw", m);
				temp.put("SFWHRW", (total2 > 0 ? "是" : "否"));
				resList.add(temp);
			}
		}
		map.put("rows", resList);
		return new DataObject(map);
	}

	/**
	 * 保存任务组定义
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public DataObject saveRwzdy(DataObject dataObject) {
		Map result = new HashMap();
		Map parameter = dataObject.getMap();
		String saveType = (String) parameter.get("SAVETYPE");
		if ("add".equals(saveType)) {
			int n = DataWindow.getTotal("timer.rwzdy.RwzdyService_countRwzmc",
					parameter);
			if (n > 0) {
				result.put(Constant.BIZ_CODE, "01");
			} else {
				String groupid = getTimerXh();
				parameter.put("GROUPID", groupid);
				DataWindow.insert("timer.rwzdy.RwzdyService_insertRwzdy",
						parameter);
				result.put(Constant.BIZ_CODE, "00");
			}
		} else if ("update".equals(saveType)) {
			DataWindow
					.update("timer.rwzdy.RwzdyService_updateRwzdy", parameter);
			result.put(Constant.BIZ_CODE, "00");
		}
		return new DataObject(result);
	}

	/**
	 * 删除任务组定义
	 */
	@SuppressWarnings({ "rawtypes" })
	public DataObject deleteRwzdy(DataObject dataObject) {
		Map result = new HashMap();
		Map parameter = dataObject.getMap();
		DataWindow dataWindow = DataWindow.query(
				"timer.rwzdy.RwzdyService_queryJobTriggerByGroupid", parameter);
		List list = dataWindow.getList();
		if (CollectionUtils.isNotEmpty(list)) {
			for (Object obj : list) {
				Map temp = (Map) obj;
				String tJobid = (String) temp.get("JOBID");
				String tGroupid = (String) temp.get("GROUPID");
				String tTrgid = (String) temp.get("TRGID");
				sjglBatchManager.delete(tJobid, tGroupid, tTrgid);
			}
		}
		// 删除jy_pcl_job_schedule中等于groupid相关的数据
		DataWindow.delete(
				"timer.rwzdy.RwzdyService_deleteJyPclJobSchByGroupid",
				parameter);
		// 删除jy_pcl_job中等于groupid相关的数据
		DataWindow.delete("timer.rwzdy.RwzdyService_deleteJyPclJobByGroupid",
				parameter);
		// 删除jy_pcl_job_group中等于groupid的数据
		DataWindow.delete(
				"timer.rwzdy.RwzdyService_deleteJyPclJobGroupByGroupid",
				parameter);
		// 删除jy_pcl_trigger_group中等于groupid的数据
		DataWindow.delete(
				"timer.rwzdy.RwzdyService_deleteJyPclTriggerGroupByGroupid",
				parameter);
		return new DataObject(result);
	}

}
