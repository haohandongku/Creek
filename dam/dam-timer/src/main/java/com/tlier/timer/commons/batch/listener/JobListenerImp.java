package com.tlier.timer.commons.batch.listener;

import java.util.Date;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.JobListener;

import com.tlier.timer.commons.constant.SrvConstants;
import com.tlier.timer.service.sjgl.SjglBatchManager;

public class JobListenerImp implements JobListener {
	private static final Log LOG = LogFactory.getLog(JobListenerImp.class);

	private String name;

	private SjglBatchManager sjglBatchManager = new SjglBatchManager();

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String getName() {
		return name;
	}

	@Override
	public void jobExecutionVetoed(JobExecutionContext paramJobExecutionContext) {
		LOG.info("jobExceution");
	}

	@Override
	public void jobToBeExecuted(JobExecutionContext paramJobExecutionContext) {
		String jobName = paramJobExecutionContext.getJobDetail().getName();
		String triName = paramJobExecutionContext.getTrigger().getName();
		if (jobName.startsWith(SrvConstants.STR_JOB)
				&& triName.startsWith(SrvConstants.STR_TRI)) {
			LOG.info("任务运行 ");
			String[] jobNameArray = jobName.split(SrvConstants.STR_SHORTLINE);
			String jobid = jobNameArray[1];
			LOG.info("任务id " + jobid);
			String[] triNameArray = triName.split(SrvConstants.STR_SHORTLINE);
			String trgid = triNameArray[1];
			LOG.info("调度id " + trgid);
			String clztDm = SrvConstants.CLZT_RUNNING;
			String clxx = "运行 ";
			Date kssj = paramJobExecutionContext.getFireTime();
			sjglBatchManager.saveJobSchedule(jobid, trgid, clztDm, clxx, kssj);
			LOG.info("任务运行记录添加 ");
		}
	}

	@Override
	public void jobWasExecuted(JobExecutionContext paramJobExecutionContext,
			JobExecutionException paramJobExecutionException) {
		String jobName = paramJobExecutionContext.getJobDetail().getName();
		String triName = paramJobExecutionContext.getTrigger().getName();
		if (jobName.startsWith(SrvConstants.STR_JOB)
				&& triName.startsWith(SrvConstants.STR_TRI)) {
			String[] jobNameArray = jobName.split(SrvConstants.STR_SHORTLINE);
			String jobid = jobNameArray[1];
			LOG.info("任务id " + jobid);
			String[] triNameArray = triName.split(SrvConstants.STR_SHORTLINE);
			String trgid = triNameArray[1];
			LOG.info("调度id " + trgid);
			String clxx = "执行结束";
			if (paramJobExecutionException != null) {
				clxx = checkNull(paramJobExecutionException.getMessage()) ? "执行结束"
						: paramJobExecutionException.getMessage();
			}
			Date kssj = paramJobExecutionContext.getFireTime();
			Date jssj = new Date();
			String clztDm = SrvConstants.CLZT_FINISHED;
			sjglBatchManager.updateToJobSch(jobid, trgid, clztDm, clxx, kssj,
					jssj);
			LOG.info("处理信息.当前任务结束时间 " + jssj.getTime());

			if (null != paramJobExecutionContext.getTrigger().getEndTime()) {
				long endTime = paramJobExecutionContext.getTrigger()
						.getEndTime().getTime();
				if (jssj.getTime() >= endTime) {
					sjglBatchManager.updateClztToTrigger(trgid, clztDm);
					LOG.info("时间调度状态的修改 ");
				}
				Date nextTime = paramJobExecutionContext.getTrigger()
						.getNextFireTime();
				if (null == nextTime) {
					sjglBatchManager.updateClztToTrigger(trgid, clztDm);
					LOG.info("时间间隔小于任务执行时间频率 ");
				}
			}
			LOG.info("任务结束 ");
		}
	}

	private static boolean checkNull(String s) {
		boolean b = false;
		if (s == null) {
			b = true;
		} else if (s.length() <= 0) {
			b = true;
		}
		return b;
	}

}
