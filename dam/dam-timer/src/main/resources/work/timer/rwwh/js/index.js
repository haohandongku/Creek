app.initHeader();
app.initSidebar("#nav_t1");
app.initFooter();
var pageSize = 10;
var serviceName = "timer.Rwwh.RwwhService";
var groupid;
var formValiator;

$(function() {
	groupid = app.getParamsFromHref().groupid;
	$("#hid_rwzid").val(groupid);

	formValiator = $("#rwForm").validate({
		rules : {
			rwms : {
				maxlength : 100
			},
			yxj : {
				maxlength : 10
			},
			rwzxt : {
				maxlength : 1000
			},
			bz : {
				maxlength : 100
			}
		}
	});

	queryRwdy(1);
});

function queryRwdy(pageNumber) {
	var param = {
		"GROUPID" : $("#hid_rwzid").val(),
		pageSize : pageSize,
		pageNumber : pageNumber
	};
	ajax(
			serviceName,
			"queryRwdy",
			param,
			function(data) {
				var total = data.total % pageSize > 0 ? (data.total / pageSize + 1)
						: (data.total / pageSize);
				if (total > 0) {
					$('#mypager').twbsPagination({
						totalPages : total,
						onPageClick : function(event, page) {
							queryRwdy(page);
						}
					});
				}

				$('#rw')
						.html(
								"<li class='title'><p class='col' style='width:8%'>序号</p><p class='col' style='width:12%'>任务名称"
										+ "</p><p class='col' style='width:10%'>优先级</p><p class='col' style='width:15%'>失败是否重做</p>"
										+ "<p class='col' style='width:15%'>任务是否阻塞</p><p class='col' style='width:30%'>任务执行体</p>"
										+ "<p class='col' style='width:10%'>操作</p></li>");
				$
						.each(
								data.rows,
								function(i, obj) {
									$('#rw')
											.append(
													"<li>"
															+ "<p class='col' style='width:8%'>"
															+ (i + 1)
															+ "</p>"
															+ "<p class='col' style='width:12%' title='" 
															+ obj.MS
															+"' >"
															+ obj.MS
															+ "</p>"
															+ "<p class='col' style='width:10%'>"
															+ obj.YXJ
															+ "</p>"
															+ "<p class='col' style='width:15%'>"
															+ converSf(obj.REDO)
															+ "</p>"
															+ "<p class='col' style='width:15%'>"
															+ converSf(obj.JOBSTATE)
															+ "</p>"
															+ "<p class='col' style='width:30%' title='" 
															+ obj.ECECUTE
															+"' >"
															+ obj.ECECUTE
															+ "</p>"
															+ "<p class='col' style='width:10%'>"
															+ "<a class='shouzhi' id='link_updateRwdy"
															+ i
															+ "' "
															+ ">编辑</a>"
															+ "&nbsp;|&nbsp;<a class='shouzhi' id='link_deleteRwdy"
															+ i
															+ "' "
															+ ">删除</a>"
															+ "</p>");
									$("#link_updateRwdy" + i).click(function() {
										updateRwdy(obj);
									});
									$("#link_deleteRwdy" + i).click(function() {
										deleteRwdy(obj);
									});
								});

			});
}

function converSf(val) {
	if (val == "Y") {
		return "是";
	} else {
		return "否";
	}
}

function updateRwdy(obj) {
	$("#rwms").attr("readonly", "readonly");
	$("#rwms").val(obj.MS);
	$("#yxj").val(obj.YXJ);
	obj.REDO == "Y" ? ($("input[name=sfSbcz]")[0].checked = true) : ($("input[name=sfSbcz]")[0].checked = false);
	obj.JOBSTATE == "Y" ? ($("input[name=sfRwzs]")[0].checked = true) : ($("input[name=sfRwzs]")[0].checked = false);
	$("#rwzxt").val(obj.ECECUTE);
	$("#bz").val(obj.BZ);
	$("#hid_rwid").val(obj.JOBID);
	formValiator.resetForm();
}

function deleteRwdy(obj) {
	  $.dialog.confirm("确定删除该任务定义吗", function() {
		var parameter = {
				"JOBID" : obj.JOBID
		};
		ajax(serviceName, "deleteRwdy", parameter, function(data) {
			if (data.RtnCode == "00") {
				$.dialog.alert("删除成功",function() {
					resetRwdy();
					queryRwdy(1);
				});				
			}
		});
	}, function() {
	});
}

function returnToRwzDy(){
	window.location.href = "../rwzdy/index.html";	
}

function saveRwdy() {
	var rwValid = $("#rwForm").valid();
	if (rwValid == false) {
		return;
	}

	var saveType = "";
	if ($("#hid_rwid").val() == "") {
		saveType = "add";
	} else {
		saveType = "update";
	}
	var parameter = {
		"GROUPID" : $("#hid_rwzid").val(),
		"JOBID" : $("#hid_rwid").val(),
		"MS" : $("#rwms").val(),
		"YXJ" : $("#yxj").val(),	
		"REDO" : $("input[name=sfSbcz]")[0].checked ? 'Y':'N',
		"JOBSTATE" : $("input[name=sfRwzs]")[0].checked ? 'Y':'N',
		"ECECUTE" : $("#rwzxt").val(),		
		"BZ" : $("#bz").val(),
		"SAVETYPE" : saveType
	};
	ajax(serviceName, "saveRwdy", parameter, function(data) {
		if (data.RtnCode == "00") {
			if (data.BizCode == "00") {
				$.dialog.alert("保存成功",function() {
					resetRwdy();
					queryRwdy(1);
				});				
			} else if (data.BizCode == "01") {
				$.dialog.alert("该任务名称已经存在,请选择其他任务名称");
			}

		}
	});
}

function resetRwdy() {
	$("#rwms").val("");
	$("#yxj").val("");
	$("input[name=sfSbcz]")[0].checked = false;
	$("input[name=sfRwzs]")[0].checked = false;
	$("#rwzxt").val("");
	$("#bz").val("");
	$("#hid_rwid").val("");
	$("#rwms").removeAttr("readonly");
}