-- ----------------------------
-- Procedure structure for `P_SEQUENCE_APP_LOG`
-- ----------------------------
DROP PROCEDURE IF EXISTS `P_SEQUENCE_APP_LOG`;
DELIMITER ;;
CREATE  PROCEDURE `P_SEQUENCE_APP_LOG`(out sequenceNo  VARCHAR(20),in sequenceName VARCHAR(20))
begin
    DECLARE ac_jdh VARCHAR(4);
    DECLARE ac_no int;
    set ac_jdh = "LOG";
    INSERT INTO SEQUENCE_APP_LOG VALUES(NULL);
    SELECT LAST_INSERT_ID() into ac_no;

    set sequenceNo = CONCAT(ac_jdh, DATE_FORMAT(CURDATE(), '%y%m%d'), lpad(CONCAT(ac_no), 6, '0'), '000');
end
;;
DELIMITER ;