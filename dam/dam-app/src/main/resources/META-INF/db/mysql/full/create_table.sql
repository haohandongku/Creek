-- ----------------------------
-- Table structure for `sequence_app_log`
-- ----------------------------
DROP TABLE IF EXISTS `sequence_app_log`;
CREATE TABLE `sequence_app_log` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '日志序号',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='日志序号';

-- ----------------------------
-- Table structure for `sys_log`
-- ----------------------------
DROP TABLE IF EXISTS `sys_log`;
CREATE TABLE `sys_log` (
  `logId` varchar(20) NOT NULL COMMENT '日志编号',
  `siteId` varchar(20) DEFAULT NULL COMMENT '站点编号',
  `userCode` varchar(20) DEFAULT NULL COMMENT '用户编码',
  `channel` varchar(100) DEFAULT NULL COMMENT '操作聚道',
  `operate` varchar(1000) NOT NULL COMMENT '操作',
  `operateObjectId` varchar(20) DEFAULT NULL COMMENT '操作对象编号',
  `operateObject` varchar(500) DEFAULT NULL COMMENT '操作对象',
  `operateIP` varchar(50) DEFAULT NULL COMMENT 'IP',
  `result` char(1) DEFAULT NULL COMMENT '结果：是否成功：Y成功，N失败',
  `content` longtext COMMENT '内容：如果是登录失败记录错误密码，其他操作记录请求及返回值、或者错误信息',
  `inputTime` datetime NOT NULL COMMENT '录入时间',
  PRIMARY KEY (`logId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='日志';
