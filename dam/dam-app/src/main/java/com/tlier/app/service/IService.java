package com.tlier.app.service;

import com.tlier.app.data.DataObject;

public interface IService {

	public DataObject doService(DataObject dataobject);

}