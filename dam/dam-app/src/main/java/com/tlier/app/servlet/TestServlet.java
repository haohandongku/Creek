package com.tlier.app.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.ArrayUtils;
import org.springframework.util.ReflectionUtils;

import com.tlier.app.constant.Constant;
import com.tlier.app.data.DataObject;
import com.tlier.app.service.BaseService;
import com.tlier.app.util.ApplicationContextUtils;
import com.tlier.db.config.CharsetConfig;

@SuppressWarnings("serial")
public class TestServlet extends HttpServlet {
	Method[] baseList = ReflectionUtils
			.getUniqueDeclaredMethods(BaseService.class);
	String[] baseNameArray;

	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);
	}

	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		String serviceName = (String) request
				.getParameter(Constant.SERVICE_NAME);
		String responseJson = "";
		try {
			if (null == baseNameArray) {
				baseNameArray = new String[baseList.length];
				for (int i = 0; i < baseList.length; i++) {
					baseNameArray[i] = baseList[i].getName();
				}
			}
			Object service = ApplicationContextUtils.getContext().getBean(
					serviceName);
			Method[] list = ReflectionUtils.getUniqueDeclaredMethods(service
					.getClass());
			List<String> methodList = new ArrayList<String>();
			for (Method method : list) {
				if (!ArrayUtils.contains(baseNameArray, method.getName())
						&& method.getReturnType() == DataObject.class) {
					methodList.add(method.getName());
				}
			}
			Map<String, Object> map = new HashMap<String, Object>();
			map.put("methodList", methodList);
			DataObject result = new DataObject(map);
			responseJson = result.getJson();
		} catch (Throwable e) {

		}
		response.setContentType("application/json");
		response.setCharacterEncoding(CharsetConfig.localCharset);
		PrintWriter out = response.getWriter();
		out.print(responseJson);
		out.flush();
		out.close();
	}
}