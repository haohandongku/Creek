package com.tlier.app.aop;

import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;
import java.io.Writer;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.aopalliance.intercept.MethodInterceptor;
import org.aopalliance.intercept.MethodInvocation;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.xml.serialize.OutputFormat;
import org.apache.xml.serialize.XMLSerializer;
import org.w3c.dom.Document;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import com.tlier.app.asynctask.AsyncTaskService;
import com.tlier.app.dao.DataWindow;
import com.tlier.app.data.DataObject;
import com.tlier.app.service.BaseService;
import com.tlier.app.util.ApplicationContextUtils;
import com.tlier.app.constant.DmSequenceName;

@SuppressWarnings("deprecation")
public class LogInterceptor extends BaseService implements MethodInterceptor {

	private static final Log LOG = LogFactory.getLog(LogInterceptor.class);
	private static DocumentBuilderFactory factory = DocumentBuilderFactory
			.newInstance();

	@Override
	public Object invoke(final MethodInvocation method) throws Throwable {
		long begin = 0;
		if (LOG.isDebugEnabled()) {
			begin = System.currentTimeMillis();
		}
		final Object result = method.proceed();
		long end = System.currentTimeMillis();
		if (LOG.isDebugEnabled()) {
			final Object target = method.getThis();
			final Object[] arguments = method.getArguments();
			final LogInterceptor instance = this;
			final long cost = end - begin;
			final String currentUserCode = getCurrentUserbm();

			AsyncTaskService.getInstance().execute(new Runnable() {

				@Override
				public void run() {
					instance.debug(result, method.getMethod(), arguments,
							target, currentUserCode, cost);
				}
			});

		}
		return result;
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	private void debug(Object result, Method mehtod, Object[] arguments,
			Object target, String currentUserCode, long cost) {
		if (LOG.isDebugEnabled()) {
			try {
				String serviceName = ApplicationContextUtils
						.getBeanNameByBeanTarget(target);
				if (StringUtils.isBlank(serviceName)) {
					serviceName = "未知ID";
				}

				StringBuffer sb = new StringBuffer();
				sb.append("服务调用结束，服务ID：" + serviceName + "，类名："
						+ target.getClass().getName() + "，方法名："
						+ mehtod.getName() + "，");

				Map parameter = new HashMap();
				String logId = getSequence(DmSequenceName.LOG_XH);
				parameter.put("logId", logId);
				StringBuffer operates = new StringBuffer();
				operates.append("服务ID：" + serviceName + "，类名："
						+ target.getClass().getName() + "，方法名："
						+ mehtod.getName());
				String userCode = currentUserCode;
				if (StringUtils.isBlank(userCode)) {
					userCode = "system";
				}
				parameter.put("userCode", userCode);
				parameter.put("operate", operates.toString());
				StringBuffer content = new StringBuffer();

				if (arguments.length > 0) {
					if (arguments.length == 1
							&& arguments[0] instanceof DataObject) {
						sb.append(" \n 输入参数：\n"
								+ format(arguments[0].toString()));
						content.append("输入参数：\n"
								+ format(arguments[0].toString()));
					}
				} else {
					sb.append(" \n 无输入参数。" + " \n 总耗时：" + cost + "毫秒。");
					content.append("无输入参数。" + " \n 总耗时：" + cost + "毫秒。");
				}

				if (result instanceof DataObject) {
					sb.append(" \n 返回值：\n" + format(result.toString())
							+ " \n 耗时：" + cost + "毫秒。");
					content.append(" \n 返回值：\n" + format(result.toString())
							+ " \n 耗时：" + cost + "毫秒。");
				}
				parameter.put("content", content.toString());
				DataWindow.insert("app.service.log.LogService_saveLog",
						parameter);
				LOG.debug(sb.toString());
			} catch (Throwable e) {
				LOG.error("日志记录时出现异常：", e);
			}
		}
	}

	private static String format(String xml) {
		xml = xml.replaceAll("&lt;", "<").replaceAll("&gt;", ">");
		try {
			Document document = parse(xml);
			OutputFormat format = new OutputFormat(document);
			format.setLineWidth(65);
			format.setIndenting(true);
			format.setIndent(2);
			Writer writer = new StringWriter();
			XMLSerializer serializer = new XMLSerializer(writer, format);
			serializer.serialize(document);
			return writer.toString();
		} catch (Exception e) {
		}
		return xml;
	}

	private static Document parse(String xml)
			throws ParserConfigurationException, SAXException, IOException {
		DocumentBuilder builder = factory.newDocumentBuilder();
		InputSource inputSource = new InputSource(new StringReader(xml));
		return builder.parse(inputSource);
	}

}