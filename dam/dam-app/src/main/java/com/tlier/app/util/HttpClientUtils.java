package com.tlier.app.util;

import org.apache.commons.httpclient.HttpClient;

public class HttpClientUtils {
	private static String proxyHost;
	private static int proxyPort;
	private static String isProxy;

	public static HttpClient getClient() {
		HttpClient client = new HttpClient();
		if ("YES".equals(isProxy)) {
			int mid = proxyHost.indexOf("|");
			String proxyHostStr1 = "";
			String proxyHostStr2 = "";
			if (mid == -1) {
				proxyHostStr1 = proxyHost;
			} else {
				proxyHostStr1 = proxyHost.substring(0, mid);
				proxyHostStr2 = proxyHost.substring(mid + 1);
			}
			try {
				client.getHostConfiguration()
						.setProxy(proxyHostStr1, proxyPort);
				client.getParams().setAuthenticationPreemptive(true);

			} catch (Exception e) {
				client.getHostConfiguration()
						.setProxy(proxyHostStr2, proxyPort);
				client.getParams().setAuthenticationPreemptive(true);
			}
		}
		return client;
	}

	public void setProxyHost(String proxyHost) {
		HttpClientUtils.proxyHost = proxyHost;
	}

	public void setProxyPort(int proxyPort) {
		HttpClientUtils.proxyPort = proxyPort;
	}

	public void setIsProxy(String isProxy) {
		HttpClientUtils.isProxy = isProxy;
	}

}
