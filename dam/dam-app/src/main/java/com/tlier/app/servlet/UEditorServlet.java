package com.tlier.app.servlet;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URLEncoder;
import java.util.Arrays;
import java.util.Date;
import java.util.Iterator;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.methods.GetMethod;
import org.apache.commons.io.IOUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.tlier.app.constant.Constant;
import com.tlier.app.data.DataObject;
import com.tlier.app.upload.Uploader;
import com.tlier.app.util.FileUtils;
import com.tlier.app.util.HttpClientUtils;

@SuppressWarnings("serial")
public class UEditorServlet extends HttpServlet {

	public static final String METHOD_NAME_FILEUP = "fileUp";
	public static final String METHOD_NAME_GETMOVIE = "getMovie";
	public static final String METHOD_NAME_GETREMOTEIMAGE = "getRemoteImage";
	public static final String METHOD_NAME_IMAGEMANAGER = "imageManager";
	public static final String METHOD_NAME_SCRAWLUP = "scrawlUp";
	public static final String METHOD_NAME_IMAGEUP = "imageUp";
	public static final String SAVE_PATH = "upload";

	private static final Log LOG = LogFactory.getLog(UEditorServlet.class);

	@Override
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);
	}

	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		try {
			Map<String, String[]> map = request.getParameterMap();
			System.out.println(map);
			String methodName = request.getParameter(Constant.METHOD_NAME);
			Uploader up = new Uploader(request);
			if (METHOD_NAME_FILEUP.equals(methodName)) {
				up.setSavePath(SAVE_PATH); // 保存路径
				String[] fileType = { ".rar", ".doc", ".docx", ".ppt", ".pptx",
						".xls", ".xlsx", ".zip", ".pdf", ".txt", ".swf",
						".wmv", ".avi", ".rm", ".rmvb", ".mpeg", ".mpg",
						".ogg", ".mov", ".wmv", ".mp4", ".gif", ".png", ".jpg",
						".jpeg", ".bmp" }; // 允许的文件类型
				up.setAllowFiles(fileType);
				up.setMaxSize(500 * 1024);// 允许的文件最大尺寸，单位KB
				up.upload();
				response.getWriter().print(
						"{'url':'" + up.getFilePath() + "','fileType':'"
								+ up.getType() + "','state':'" + up.getState()
								+ "','original':'"
								+ up.getParameter("Filename") + "'}");
			} else if (METHOD_NAME_IMAGEUP.equals(methodName)) {
				if (request.getParameter("fetch") != null||up.getParameter("fetch") != null) {
					response.setHeader("Content-Type", "text/javascript");
					String dirs = "[";
					dirs += "'" + SAVE_PATH + "'";
					dirs += "]";
					response.getWriter().print(
							"updateSavePath( " + dirs + " );");
					return;
				}
				up.setSavePath(SAVE_PATH);// 保存路径
				String[] fileType = { ".gif", ".png", ".jpg", ".jpeg", ".bmp" };
				up.setAllowFiles(fileType);
				up.upload();
				response.getWriter().print(
						"{'original':'" + up.getParameter("Filename")
								+ "','url':'" + up.getFilePath()
								+ "','title':'" + up.getTitle() + "','state':'"
								+ up.getState() + "'}");
			} else if (METHOD_NAME_GETMOVIE.equals(methodName)) {
				StringBuffer readOneLineBuff = new StringBuffer();
				String content = "";
				String searchkey = up.getParameter("searchKey");
				String videotype = up.getParameter("videoType");
				try {
					searchkey = URLEncoder.encode(searchkey, "utf-8");
					HttpClient client = HttpClientUtils.getClient();
					GetMethod get = new GetMethod(
							"http://api.tudou.com/v3/gw?method=item.search&appKey=myKey&format=json&kw="
									+ searchkey
									+ "&pageNo=1&pageSize=20&channelId="
									+ videotype + "&inDays=7&media=v&sort=s");
					client.executeMethod(get);
					BufferedReader reader = new BufferedReader(
							new InputStreamReader(
									get.getResponseBodyAsStream(), "utf-8"));
					String line = "";
					while ((line = reader.readLine()) != null) {
						readOneLineBuff.append(line);
					}
					content = readOneLineBuff.toString();
					reader.close();
				} catch (MalformedURLException e) {
				} catch (IOException e2) {
				}
				response.getWriter().print(content);
			} else if (METHOD_NAME_GETREMOTEIMAGE.equals(methodName)) {
				String srcurl = up.getParameter("upfile");
				String state = "远程图片抓取成功！";
				String[] arr = srcurl.split("ue_separate_ue");
				String[] outSrc = new String[arr.length];
				for (int i = 0; i < arr.length; i++) {

					/*// 格式验证
					String type = getFileType(arr[i]);
					if (type.equals("")) {
						state = "图片类型不正确！";
						continue;
					}*/
					// 大小验证
					HttpClient client = HttpClientUtils.getClient();
					GetMethod get = new GetMethod(arr[i]);
					client.executeMethod(get);
					if (get.getStatusCode() != 200) {
						state = "请求地址不存在！";
						continue;
					}
					try {
						DataObject data = FileUtils.upLoadFile(IOUtils
								.toByteArray(get.getResponseBodyAsStream()), up
								.getFolder(SAVE_PATH),
								Long.toString(new Date().getTime()));
						outSrc[i] = (String) data.getMap().get("URL");
					} catch (Exception e) {
					}
				}
				String outstr = "";
				for (int i = 0; i < outSrc.length; i++) {
					outstr += outSrc[i] + "ue_separate_ue";
				}
				outstr = outstr.substring(0,
						outstr.lastIndexOf("ue_separate_ue"));
				response.getWriter().print(
						"{'url':'" + outstr + "','tip':'" + state
								+ "','srcUrl':'" + srcurl + "'}");
			} else if (METHOD_NAME_IMAGEMANAGER.equals(methodName)) {
			} else if (METHOD_NAME_SCRAWLUP.equals(methodName)) {
				String path = "upload";
				up.setSavePath(path);
				String[] fileType = { ".gif", ".png", ".jpg", ".jpeg", ".bmp" };
				up.setAllowFiles(fileType);
				up.setMaxSize(10000);
				up.uploadBase64("content");
				response.getWriter().print(
						"{'url':'" + up.getUrl() + "',state:'" + up.getState()
								+ "'}");
			} else {
				response.sendError(HttpServletResponse.SC_NOT_FOUND);
			}
		} catch (Throwable e) {
			LOG.error("文件上传时出现异常：", e);
		}
	}

	public String getFileType(String fileName) {
		if(0<fileName.indexOf("?")){
    		fileName = fileName.substring(0,fileName.indexOf("?"));
    	}
		String[] fileType = { ".gif", ".png", ".jpg", ".jpeg", ".bmp" };
		Iterator<String> type = Arrays.asList(fileType).iterator();
		while (type.hasNext()) {
			String t = type.next();
			if (fileName.endsWith(t)) {
				return t;
			}
		}
		return "";
	}
}
