package com.tlier.app.util;

/**
 * 用户名线程变量
 * 
 * @author zhongym
 * 
 */
public class UserNameThreadLocal {

	private static ThreadLocal<String> userNameThreadLocal = new ThreadLocal<String>();

	public static void set(String userName) {
		userNameThreadLocal.set(userName);
	}

	public static String get() {
		return userNameThreadLocal.get();
	}

	public static void remove() {
		userNameThreadLocal.remove();
	}

}