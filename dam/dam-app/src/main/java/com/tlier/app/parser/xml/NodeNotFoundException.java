package com.tlier.app.parser.xml;

import com.tlier.app.exception.BizRuntimeException;

@SuppressWarnings("serial")
public class NodeNotFoundException extends BizRuntimeException {
}