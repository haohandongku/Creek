package com.tlier.app.constant;

/**
 * 序号生成器
 */
public class DmSequenceName {

	/** 通用序号 */
	public static final String XH = "SEQUENCE_XH";
	/**日志序号*/
	public static final String LOG_XH = "SEQUENCE_APP_LOG";

}