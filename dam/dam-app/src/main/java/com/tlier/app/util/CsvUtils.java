package com.tlier.app.util;

import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Writer;
import java.util.List;

import com.opencsv.CSVReader;
import com.opencsv.CSVWriter;

public class CsvUtils {

	public static void createCsv(String[] titles, List<String[]> datas,
			Writer writer) throws Exception {
		CSVWriter csvWriter = new CSVWriter(writer, ',');
		if (null != titles && 0 < titles.length) {
			csvWriter.writeNext(titles);
		}
		for (String[] values : datas) {
			csvWriter.writeNext(values);
		}
		csvWriter.close();
	}

	public static List<String[]> getCsvData(InputStreamReader reader,
			boolean withTitles) throws IOException {
		CSVReader csvReader = new CSVReader(reader);
		if (!withTitles) {
			csvReader.readNext();
		}
		List<String[]> list = csvReader.readAll();
		csvReader.close();
		return list;
	}

}