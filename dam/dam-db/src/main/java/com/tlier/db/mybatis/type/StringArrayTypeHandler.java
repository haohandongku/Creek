package com.tlier.db.mybatis.type;

import java.io.UnsupportedEncodingException;
import java.sql.CallableStatement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.apache.ibatis.type.BaseTypeHandler;
import org.apache.ibatis.type.JdbcType;

import com.tlier.db.config.CharsetConfig;

public class StringArrayTypeHandler extends BaseTypeHandler<String[]> {

	private static final String SEPARATOR = ",";

	@Override
	public void setNonNullParameter(PreparedStatement ps, int i,
			String[] parameter, JdbcType jdbcType) throws SQLException {
		StringBuffer sb = new StringBuffer();
		try {
			for (String item : parameter) {
				String value = new String(
						item.getBytes(CharsetConfig.localCharset),
						CharsetConfig.dbCharset);
				sb.append(value).append(SEPARATOR);
			}
		} catch (UnsupportedEncodingException e) {
			throw new SQLException(e);
		}
		sb.deleteCharAt(sb.length() - 1);
		ps.setString(i, sb.toString());
	}

	@Override
	public String[] getNullableResult(ResultSet rs, String columnName)
			throws SQLException {
		try {
			return getStringArray(rs.getString(columnName));
		} catch (UnsupportedEncodingException e) {
			throw new SQLException(e);
		}
	}

	@Override
	public String[] getNullableResult(ResultSet rs, int columnIndex)
			throws SQLException {
		try {
			return getStringArray(rs.getString(columnIndex));
		} catch (UnsupportedEncodingException e) {
			throw new SQLException(e);
		}
	}

	@Override
	public String[] getNullableResult(CallableStatement cs, int columnIndex)
			throws SQLException {
		try {
			return getStringArray(cs.getString(columnIndex));
		} catch (UnsupportedEncodingException e) {
			throw new SQLException(e);
		}
	}

	private String[] getStringArray(String columnValue)
			throws UnsupportedEncodingException {
		if (columnValue == null) {
			return null;
		}
		String value = new String(
				columnValue.getBytes(CharsetConfig.dbCharset),
				CharsetConfig.localCharset);
		return value.split(SEPARATOR);
	}

}