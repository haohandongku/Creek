package com.tlier.db.mybatis.type;

import java.io.StringReader;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Method;
import java.sql.CallableStatement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import oracle.sql.CLOB;

import org.apache.ibatis.type.ClobTypeHandler;
import org.apache.ibatis.type.JdbcType;

import com.tlier.db.config.CharsetConfig;

public class WeblogicClobTypeHandlerSub extends ClobTypeHandler {

	@Override
	public void setNonNullParameter(PreparedStatement ps, int i,
			String parameter, JdbcType jdbcType) throws SQLException {
		String value = null;
		try {
			value = new String(parameter.getBytes(CharsetConfig.localCharset),
					CharsetConfig.dbCharset);
		} catch (UnsupportedEncodingException e) {
			throw new SQLException(e);
		}
		ps.setCharacterStream(i, new StringReader(value), value.length());
	}

	@Override
	public String getNullableResult(ResultSet rs, String columnName)
			throws SQLException {
		return getValue(rs.getObject(columnName));
	}

	@Override
	public String getNullableResult(ResultSet rs, int columnIndex)
			throws SQLException {
		return getValue(rs.getObject(columnIndex));
	}

	@Override
	public String getNullableResult(CallableStatement cs, int columnIndex)
			throws SQLException {
		return getValue(cs.getObject(columnIndex));
	}

	private String getValue(Object clob) throws SQLException {
		try {
			if ("weblogic.jdbc.wrapper.Clob_oracle_sql_CLOB".equals(clob
					.getClass().getName())) {
				Method method = clob.getClass().getMethod("getVendorObj",
						new Class[] {});
				CLOB temp = (CLOB) method.invoke(clob);
				if (temp != null) {
					String value = temp.getSubString(1L, (int) temp.length());
					return new String(value.getBytes(CharsetConfig.dbCharset),
							CharsetConfig.localCharset);
				}
			}
		} catch (Exception e) {
			throw new SQLException(e);
		}
		return null;
	}

}