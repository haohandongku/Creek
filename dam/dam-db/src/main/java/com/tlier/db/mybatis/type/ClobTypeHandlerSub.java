package com.tlier.db.mybatis.type;

import java.io.StringReader;
import java.io.UnsupportedEncodingException;
import java.sql.CallableStatement;
import java.sql.Clob;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.apache.ibatis.type.ClobTypeHandler;
import org.apache.ibatis.type.JdbcType;

import com.tlier.db.config.CharsetConfig;

public class ClobTypeHandlerSub extends ClobTypeHandler {

	public void setNonNullParameter(PreparedStatement ps, int i,
			String parameter, JdbcType jdbcType) throws SQLException {
		String value = null;
		try {
			value = new String(parameter.getBytes(CharsetConfig.localCharset),
					CharsetConfig.dbCharset);
		} catch (UnsupportedEncodingException e) {
			throw new SQLException(e);
		}
		ps.setCharacterStream(i, new StringReader(value), value.length());
	}

	public String getNullableResult(ResultSet rs, String columnName)
			throws SQLException {
		return getValue(rs.getClob(columnName));
	}

	public String getNullableResult(ResultSet rs, int columnIndex)
			throws SQLException {
		return getValue(rs.getClob(columnIndex));
	}

	public String getNullableResult(CallableStatement cs, int columnIndex)
			throws SQLException {
		return getValue(cs.getClob(columnIndex));
	}

	private String getValue(Clob clob) throws SQLException {
		if (clob != null) {
			String value = clob.getSubString(1L, (int) clob.length());
			try {
				return new String(value.getBytes(CharsetConfig.dbCharset),
						CharsetConfig.localCharset);
			} catch (UnsupportedEncodingException e) {
				throw new SQLException(e);
			}
		}
		return null;
	}

}