package com.tlier.db.mybatis.type;

import java.sql.CallableStatement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.apache.ibatis.type.BaseTypeHandler;
import org.apache.ibatis.type.JdbcType;

public class BooleanTypeHandlerSub extends BaseTypeHandler<Boolean> {

	@Override
	public Boolean getNullableResult(CallableStatement cs, int index)
			throws SQLException {
		return this.getValue(cs.getString(index));
	}

	@Override
	public Boolean getNullableResult(ResultSet rs, int index)
			throws SQLException {
		return this.getValue(rs.getString(index));
	}

	@Override
	public Boolean getNullableResult(ResultSet rs, String str)
			throws SQLException {
		return this.getValue(rs.getString(str));
	}

	@Override
	public void setNonNullParameter(PreparedStatement ps, int i,
			Boolean parameter, JdbcType jdbcType) throws SQLException {
		String value = (Boolean) parameter == true ? "Y" : "N";
		ps.setString(i, value);
	}

	private Boolean getValue(String value) {
		if ("Y".equalsIgnoreCase(value)) {
			return Boolean.TRUE;
		}
		return Boolean.FALSE;
	}

}