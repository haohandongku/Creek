package com.tlier.db.mybatis.type;

import java.sql.PreparedStatement;
import java.sql.SQLException;

import org.apache.ibatis.type.IntegerTypeHandler;
import org.apache.ibatis.type.JdbcType;

public class SafeIntegerTypeHandler extends IntegerTypeHandler {

	public void setNonNullParameter(PreparedStatement ps, int i,
			Integer parameter, JdbcType jdbcType) throws SQLException {
		if (JdbcType.VARCHAR == jdbcType || JdbcType.CHAR == jdbcType
				|| JdbcType.LONGVARCHAR == jdbcType
				|| JdbcType.NVARCHAR == jdbcType || JdbcType.NCHAR == jdbcType) {
			ps.setString(i, String.valueOf(parameter.intValue()));
		} else {
			super.setNonNullParameter(ps, i, parameter, jdbcType);
		}
	}
}